<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Fbs
 * @property \Cake\ORM\Association\HasMany $Addresses
 * @property \Cake\ORM\Association\HasMany $Bussinesses
 */
class UsersTable extends Table {

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->table('users');
    $this->displayField('id');
    $this->primaryKey('id');

    // $this->belongsTo('Fbs', [
    //     'foreignKey' => 'fb_id'
    // ]);
    $this->hasMany('Addresses', [
      'foreignKey' => 'user_id',
    ]);
    $this->hasMany('Bussinesses', [
      'foreignKey' => 'user_id',
    ]);
    $this->hasMany('UserRanks', [
      'foreignKey' => 'user_id',
    ]);
    $this->addBehavior('Image',[
      'field_name' => 'profile_image'
    ]);
    $this->belongsToMany('Products', [
      'through' => 'UserRanks',
      'foreignKey' => 'user_id',
      'conditions' => ['relation_type' => "Products"]
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
      ->allowEmpty('first_name');

    $validator
      ->allowEmpty('last_name');

    $validator
      ->add('email', 'valid', ['rule' => 'email'])
      ->notEmpty('email');
    $validator
      ->allowEmpty('username');

    $validator
      ->notEmpty('password');
     $validator
      ->notEmpty('confirm_password');

    $validator
      ->allowEmpty('old_password');

    $validator
      ->allowEmpty('acess_token');
    $validator
      ->allowEmpty('token');

    $validator
      ->allowEmpty('client');

    $validator
      ->add('expiry', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('expiry');

    $validator
      ->add('birthday', 'valid', ['rule' => 'date'])
      ->allowEmpty('birthday');

    $validator
      ->add('active', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('active');

    $validator
      ->add('created_at', 'valid', ['rule' => 'datetime'])
      ->allowEmpty('created_at');

    $validator
      ->add('updated_at', 'valid', ['rule' => 'datetime'])
      ->allowEmpty('updated_at');
    
    $validator->add('password', 'no-misspelling', [
      'rule' => ['compareWith', 'confirm_password'],
      'message' => 'Passwords are not equal',
    ]);

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
    $rules->add($rules->isUnique(['email']));

    return $rules;
  }

}
