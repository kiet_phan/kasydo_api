<?php

namespace App\Model\Table;

use App\Model\Entity\Bussiness;
use App\Model\Entity\Image;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
//use Cake\Event\Event;

/**
 * Bussinesses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $BussinessSubTypes
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Products
 */
class BussinessesTable extends Table {

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->table('bussinesses');
    $this->displayField('name');
    $this->primaryKey('id');

    $this->belongsTo('BussinessSubTypes', [
      'foreignKey' => 'bussiness_sub_type_id',
    ]);
    $this->belongsTo('Users', [
      'foreignKey' => 'user_id',
    ]);
    $this->hasMany('Products', [
      'foreignKey' => 'bussiness_id',
    ]);
    $this->hasMany('Images', [
      'className' => 'Images',
      'foreignKey' => 'foreign_key',
      'conditions' => ['model' => "Bussinesses"],
    ]);
    $this->hasOne('FirstImage', [
      'className' => 'Images',
      'foreignKey' => 'foreign_key',
      'conditions' => function ($e, $query) {
        $query->where('model = "Bussinesses"')->limit(1);
        return [];
      },
    ]);
    $this->hasMany('Comments', [
      'className' => 'Comments',
      'foreignKey' => 'commentable_id',
      'conditions' => ['commentable_type' => "Bussinesses"],
    ]);

    $this->hasMany('TopComments', [
      'className' => 'Comments',
      'foreignKey' => 'commentable_id',
      'conditions' => function ($e, $query) {
        $query->where('commentable_type = "Bussinesses"')->limit(2);
        return [];
      },
    ]);

    $this->hasMany('UserRanks', [
      'className' => 'UserRanks',
      'foreignKey' => 'relation_id',
      'conditions' => ['relation_type' => "Bussinesses"]
    ]);
    $this->belongsToMany('UserRates', [
      'className' => 'Users',
      'through' => 'UserRanks',
      'foreignKey' => 'relation_id',
      'conditions' => ['relation_type' => "Bussinesses"]
    ]);

    $this->addBehavior('Image');
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
      ->notEmpty('name');

    $validator
      ->notEmpty('latitude');

    $validator
      ->notEmpty('longitude');

    $validator
      ->add('created_at', 'valid', ['rule' => 'datetime'])
      ->allowEmpty('created_at');

    $validator
      ->add('updated_at', 'valid', ['rule' => 'datetime'])
      ->allowEmpty('updated_at');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
//    $rules->add($rules->existsIn(['bussiness_sub_type_id'], 'BussinessSubTypes'));
    //    $rules->add($rules->existsIn(['user_id'], 'Users'));
    return $rules;
  }

  public function getNearBy($lat, $long,$conditions =[],$current_user_id =  0) {
    $query = $this->find('all');

    $queryDistance = "( 6371 * acos( cos( radians($lat) ) * cos( radians(latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) )";
    $query->select(['id','user_id',
      'distance' => $query->newExpr($queryDistance)
    ])
    ->having(['distance <=' => 1])
    ->contain(['BussinessSubTypes','Users','FirstImage',
      'UserRanks'=> function ($q) {
        return $q
          ->select(['relation_id',
            'total_user_rate' => $q->func()->count('user_id'),
            'total_rank'=>$q->func()->sum('rank_value')
          ])
          ->group('relation_id');
      },
      'UserRates'=>function ($q) use($current_user_id) {
        return $q->where(['UserRates.id'=>$current_user_id]);
      }
    ])
    ->where($conditions)
    ->autoFields(true);
    return $query;
  }


}
//( 3959 * acos( cos( radians(:latitude) ) * cos( radians(latitude ) ) * cos( radians( longitude ) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians( latitude ) ) ) )
//AS distance
