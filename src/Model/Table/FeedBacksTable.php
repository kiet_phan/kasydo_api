<?php
namespace App\Model\Table;

use App\Model\Entity\FeedBack;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * FeedBacks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class FeedBacksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->table('feed_backs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Images', [
          'className' => 'Images',
          'foreignKey' => 'foreign_key',
          'conditions' => ['model' => "FeedBacks"],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('summary', 'create')
            ->notEmpty('summary');

        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->allowEmpty('created_at');

        $validator
            ->allowEmpty('modified_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
    public function afterSave(Event $event, Entity $entity, $options) {
        if (isset($entity['image_list'])) {
            foreach ($entity['image_list'] as $img_id) {
                $images = TableRegistry::get('images');
                $image = $images->get($img_id);
                if (empty($image->foreign_key)) {
                    $image->foreign_key = $entity->id;
                    $image->model = 'FeedBacks';
                    $images->save($image);
                }
            }
        }
    }
}
