<?php

namespace App\Model\Table;

use App\Model\Entity\Image;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;

/**
 * Images Model
 *
 */
class ImagesTable extends Table {

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->table('images');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
//    $this->belongsTo('Bussinesses', [
//      'foreignKey' => 'foreign_key',
//      'joinType' => 'INNER',
//      'conditions' => ['Images.model' => 'Bussinesses'],
//    ]);
    $this->belongsTo('Products', [
      'foreignKey' => 'foreign_key',
      'joinType' => 'INNER',
      'conditions' => ['Images.model' => 'Products'],
    ]);

    $this->belongsTo('FeedBacks', [
      'foreignKey' => 'foreign_key',
      'joinType' => 'INNER',
      'conditions' => ['Images.model' => 'FeedBacks'],
    ]);
    $this->addBehavior('Image');
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    // $validator
    //   ->add('foreign_key', 'valid', ['rule' => 'numeric'])
    //   ->requirePresence('foreign_key', 'create')
    //   ->notEmpty('foreign_key');

    // $validator
    //   ->requirePresence('model', 'create')
    //   ->notEmpty('model');
    //
    // $validator
    //   ->requirePresence('field', 'create')
    //   ->notEmpty('field');
    //
    // $validator
    //   ->requirePresence('filename', 'create')
    //   ->notEmpty('filename');
    //
    // $validator
    //   ->requirePresence('mime', 'create')
    //   ->notEmpty('mime');
    //
    // $validator
    //   ->add('size', 'valid', ['rule' => 'numeric'])
    //   ->requirePresence('size', 'create')
    //   ->notEmpty('size');

    return $validator;
  }
 


}
