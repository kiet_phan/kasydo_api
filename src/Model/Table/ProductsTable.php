<?php

namespace App\Model\Table;

use App\Model\Entity\Product;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Bussinesses
 */
class ProductsTable extends Table {

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config) {
    parent::initialize($config);

    $this->table('products');
    $this->displayField('title');
    $this->primaryKey('id');

    $this->belongsTo('Bussinesses', [
      'foreignKey' => 'bussiness_id'
    ]);
    $this->hasMany('Images', [
      'className' => 'Images',
      'foreignKey' => 'foreign_key',
      'conditions' => ['model' => "Products"],
    ]);
    $this->hasOne('FirstImage', [
      'className' => 'Images',
      'foreignKey' => 'foreign_key',
      'conditions' => ['model' => "Products"],
    ]);
     $this->hasMany('Comments', [
      'className' => 'Comments',
      'foreignKey' => 'commentable_id',
      'conditions' => ['commentable_type' => "Products"]
    ]);

    $this->hasMany('TopComments', [
      'className' => 'Comments',
      'foreignKey' => 'commentable_id',
      'conditions' => function ($e, $query) {
        $query->where('commentable_type = "Products"')->limit(2);
        return [];
      },
    ]);

    $this->hasMany('UserRanks', [
      'className' => 'UserRanks',
      'foreignKey' => 'relation_id',
      'conditions' => ['relation_type' => "Products"]
    ]);
    $this->belongsToMany('Users', [
      'through' => 'UserRanks',
      'foreignKey' => 'relation_id',
      'conditions' => ['relation_type' => "Products"]
    ]);

    $this->belongsToMany('UserRates', [
      'className' => 'Users',
      'through' => 'UserRanks',
      'foreignKey' => 'relation_id',
      'conditions' => ['relation_type' => "Products"]
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) {
    $validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
      ->allowEmpty('title');

    $validator
      ->add('rating', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('rating');

    $validator
      ->add('created_at', 'valid', ['rule' => 'datetime'])
      ->allowEmpty('created_at');

    $validator
      ->add('updated_at', 'valid', ['rule' => 'datetime'])
      ->allowEmpty('updated_at');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
   * @return \Cake\ORM\RulesChecker
   */
  public function buildRules(RulesChecker $rules) {
    $rules->add($rules->existsIn(['bussiness_id'], 'Bussinesses'));
    return $rules;
  }

  public function afterSave(Event $event, Entity $entity, $options) {
    if (isset($entity['image_list'])) {
      foreach ($entity['image_list'] as $img) {
        $images = TableRegistry::get('images');
        $image = $images->get($img->id);
        if (empty($image->foreign_key)) {
          $image->description = $img->description;
          $image->foreign_key = $entity->id;
          $image->model = 'Products';
          $images->save($image);
        }
      }
    }
  }
  public function getProductByDistance($lat,$long,$conditions = [],$current_user_id = 0){
    $containTable = [
      'Comments' => [
        'queryBuilder' => function ($q) {
          return $q->order(['Comments.created_at' =>'DESC'])
            ->limit(2);
        },
        'Users' => [
          'queryBuilder' => function ($q) {
            return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
          }
        ],

      ],
      'Images' => [
        'queryBuilder' => function ($q) {
          return $q->limit(1);
        }
      ],
      'Users' => function ($q) use($current_user_id) {
        return $q->where(['Users.id'=>$current_user_id]);
      },
      'UserRates'=>function ($q) use($current_user_id) {
        return $q->where(['UserRates.id'=>$current_user_id]);
      }
    ];


    $query = $this->find();
    $queryDistance = "( 6371 * acos( cos( radians($lat) ) * cos( radians(Bussinesses.latitude ) ) * cos( radians( Bussinesses.longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( Bussinesses.latitude ) ) ) )";
    $query->select([
      'distance' => $query->newExpr($queryDistance)
    ])
    ->innerJoinWith('Bussinesses.BussinessSubTypes')
    ->having(['distance <=' => 1])
    ->where($conditions)
    ->contain($containTable)
    ->autoFields(true);

    return $query;
  }

}
