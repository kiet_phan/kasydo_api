<?php

namespace App\Model\Behavior;

use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\I18n\Time;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use WideImage\WideImage;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;
use Cake\Datasource\ConnectionManager;

/**
 * ImageBehavior.
 *
 * @package Image\Model\Behavior
 * @author Krzysztof Karolak
 * @copyright 2015
 * @version 1.0
 */
class ImageBehavior extends Behavior {

  /**
   * Images table.
   *
   * @var Table
   */
  protected $_imagesTable;
  protected $_field;
  protected $_field_name;

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [
    /**
     * Table name used to save image data.
     */
    'table' => 'Images',
    /**
     * Base path where to put all model images.
     */
    'path' => null,
    /**
     * Array of fields as 'field type' => 'cake association type'.
     */
    'fields' => 'imagefile',
    'field_name' => 'image_name',
    /**
     * Other images to generate from source.
     * Image manipulation is handled by WideImage library.
     * Each array key is a directory name and values are arrays as 'operation' => 'params'.
     */
    'presets' => [],
    /**
     * Image quality for all presets.
     */
    'quality' => 100
  ];

  /**
   * Map field type used in config "fields" to cake association.
   *
   * @var array
   */

  /**
   * Behavior initialize method.
   *
   * @param array $config Config array.
   * @return void
   */
  public function initialize(array $config) {
    $this->_imagesTable = $this->config('table');
    $this->_field = $this->config('fields');
    $this->_field_name =  $this->config('field_name');

    // $this->_imagesTable->eventManager()->on('Model.afterDelete', [$this, 'imageAfterDelete']);
    // $this->setupAssociations($this->config('table'), $this->config('fields'));
  }

  /**
   * Setup table associations.
   *
   * @param string $imagesTableName Table name for images.
   * @param array $fields Array as 'fieldName' => 'fieldType'.
   * @return void
   */

  /**
   * Implementation of the beforeSave event, handles uploading / saving and overwriting of image records.
   *
   * @param Event $event Event object.
   * @param Entity $entity Entity object.
   * @param \ArrayObject $options Options array.
   * @return void
   */
  
  /**
   * Implementation of afterSave event, handles generating additional files.
   *
   * @param Event $event Event object.
   * @param Entity $entity Entity object.
   * @param \ArrayObject $options Options array.
   * @return void
   */
  public function afterSave(Event $event, Entity $entity, $options) {
//    $forder = new Folder(IMAGE_DIR);
    if (isset($entity[$this->_field]) && empty($entity[$this->_field]['error'])) {
      
      $image = $entity[$this->_field];
      $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
      $image['name'] = md5(time() . $image['name']) . '.' . $extension;
      $this->_upload($image, $entity->id, $entity->isNew());
      $connection = ConnectionManager::get('default');
      $connection->update($this->_table->table(), [$this->_field_name => $image['name']], ['id' => $entity->id]);

      unset($entity[$this->_field]);
    }else{
      return false;
    }
  }

  protected function _upload($img, $id, $isNew = true) {

    $alias = $this->_table->alias();
    $basePath = IMAGE_DIR . DS . strtolower($alias) . DS . $id;
    $extension = pathinfo($img['name'], PATHINFO_EXTENSION);
    $imagePath = $basePath . DS . $img['name'];
    $quality = $this->getCorrectQuality($extension, 100);
    if (!$isNew) {
      $forder = new Folder($basePath);
      if ($forder->path) {
        $forder->delete();
      }
    }

    $forder = new Folder($basePath, true);
    $imagine = new Imagine();
    $image = $imagine->open($img['tmp_name']);
    $size = $image->getSize();
    $height = $size->getHeight();
    $wight = $size->getWidth();
    // $size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight()
    $image->save($imagePath, $quality);
    $image->resize($wight > $height ? new Box($height, $height) : new Box($wight, $wight) )
      ->save($basePath . DS . 'big_' .  $img['name'], $quality);
  }

  /**
   * Implementation of afterDelete event for images table, handles deleting image files.
   *
   * @param Event $event Event object.
   * @param Entity $entity Entity object.
   * @param \ArrayObject $options Options array.
   * @return void
   */
  public function afterDelete(Event $event, Entity $entity, \ArrayObject $options) {
    $alias = $this->_table->alias();
    $basePath = IMAGE_DIR . DS . strtolower($alias) . DS . $entity->id;
    $forder = new Folder($basePath);
    if ($forder->path) {
      $forder->delete();
    }
  }

  /**
   * Upload function.
   *
   * @param string $fileName Original name of the tmp file.
   * @param string $filePath Full path to the tmp file.
   * @param bool $copy Whether copy or move the tmp file.
   * @return array
   */

  /**
   * Generate source image presets.
   *
   * @param Entity $imageEntity Image entity.
   * @param bool $force Use force to override image if exists.
   * @return void
   */
  public function generatePresets($imageEntity, $force = false) {
    $basePath = $this->basePath($imageEntity->model) . DS;
    $imagePath = $basePath . $imageEntity->filename;
    $quality = $this->getCorrectQuality(pathinfo($imagePath, PATHINFO_EXTENSION));

    $presets = $this->config('presets');
    foreach ($presets as $preset => $options) {
      $dir = $basePath . $preset . DS;
      $folder = new Folder($dir, true, 0775);
      $destination = $dir . $imageEntity->filename;

      if (!$force && file_exists($destination)) {
        continue;
      }

      $wImage = WideImage::load($imagePath);
      foreach ($options as $action => $params) {
        if (is_callable($params)) {
          $wImage = $params($wImage, $imagePath);
        } else {
          $wImage = call_user_func_array([$wImage, $action], $params);
        }
      }

      $wImage->saveToFile($destination, $quality);
    }
  }

  /**
   * Generates unique filename.
   *
   * @param string $fileName Name of the file.
   * @param string $filePath File path.
   * @return string
   */
  public function generateUniqueFilename($fileName, $filePath) {
    return md5_file($filePath) . '.' . pathinfo($fileName, PATHINFO_EXTENSION);
  }

  /**
   * Return base path for current model or override by 'alias' parameter.
   *
   * @param null|string $alias Optional name for model directory in base path.
   * @return string
   */
  public function basePath($alias = null) {
    if (!$alias) {
      $alias = $this->_table->alias();
    }

    return $this->config('path') . DS . $alias;
  }

  /**
   * Get correct quality to file extension.
   * Default is 0-100, but some files (like .png) needs 0-9 scale.
   *
   * @param string $extension File extension returned from pathinfo().
   * @return int
   */
  private function getCorrectQuality($extension, $quality) {
    $quality_arr = ['jpeg_quality' => $quality];
    if (strtolower($extension) == 'png') {
      $quality = 9 - round(($quality / 100) * 9);
      $quality_arr = ['png_compression_level' => $quality];
    }

    return $quality_arr;
  }

}
