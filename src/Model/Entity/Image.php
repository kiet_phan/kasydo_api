<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Image Entity.
 *
 * @property int $id
 * @property int $foreign_key
 * @property string $model
 * @property string $field
 * @property string $filename
 * @property string $mime
 * @property int $size
 * @property \Cake\I18n\Time $created
 */
class Image extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    public $imagefile;
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
