<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $old_password
 * @property string $acess_token
 * @property string $token
 * @property string $client
 * @property float $expiry
 * @property string $fb_id
 * @property \App\Model\Entity\Fb $fb
 * @property \Cake\I18n\Time $birthday
 * @property int $active
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\Bussiness[] $bussinesses
 */
class User extends Entity {

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * Note that when '*' is set to true, this allows all unspecified fields to
	 * be mass assigned. For security purposes, it is advised to set '*' to false
	 * (or remove it), and explicitly make individual fields accessible as needed.
	 *
	 * @var array
	 */
  protected $_virtual = ['confirm_password','imagefile'];
	protected $_accessible = [
		'*' => true,
		'id' => false,
	];
  protected $_hidden = ['password','confirm_password','imagefile'];
	protected function _setPassword($password) {
		return (new DefaultPasswordHasher)->hash($password);
	}
}
