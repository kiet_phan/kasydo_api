<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserRank Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $relation_id
 * @property \App\Model\Entity\Relation $relation
 * @property string $relation_type
 * @property string $rank_number
 */
class UserRank extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
