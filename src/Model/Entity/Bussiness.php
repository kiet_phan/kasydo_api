<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bussiness Entity.
 *
 * @property int $id
 * @property int $bussiness_sub_type_id
 * @property \App\Model\Entity\BussinessSubType $bussiness_sub_type
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $name
 * @property string $latitude
 * @property string $longitude
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property \App\Model\Entity\Product[] $products
 */
class Bussiness extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    public $imagefile;
    protected $_virtual = ['first_image'];
    protected $_hidden = ['created_at','updated_at'];
    protected $_accessible = [
      '*' => true,
      'id' => false,
      'user_ranks' => true
    ];
}
