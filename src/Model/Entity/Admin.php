<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Admin Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $description
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 */
class Admin extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_virtual = ['confirm_password','imagefile'];
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected $_hidden = ['password','confirm_password','imagefile'];
    protected function _setPassword($password) {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
