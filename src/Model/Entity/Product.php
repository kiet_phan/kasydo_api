<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity.
 *
 * @property int $id
 * @property string $title
 * @property int $rating
 * @property int $bussiness_id
 * @property \App\Model\Entity\Bussiness $bussiness
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    public $image_list;
    public $count_comment;
    protected $_virtual = ['first_image'];
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'user_ranks' => true
    ];
}
