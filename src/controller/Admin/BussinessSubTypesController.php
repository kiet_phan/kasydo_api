<?php
namespace App\Controller\Admin;

use App\Controller\BackendController;

/**
 * BussinessSubTypes Controller
 *
 * @property \App\Model\Table\BussinessSubTypesTable $BussinessSubTypes
 */
class BussinessSubTypesController extends BackendController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['BussinessTypes']
        ];
        $this->set('bussinessSubTypes', $this->paginate($this->BussinessSubTypes));
    }
    public function isAuthorized($user)
    {
        // The owner of an article can edit and delete it

        return true ;
    }

    /**
     * View method
     *
     * @param string|null $id Bussiness Sub Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bussinessSubType = $this->BussinessSubTypes->get($id, [
            'contain' => ['BussinessTypes', 'Bussinesses']
        ]);
        $this->set('bussinessSubType', $bussinessSubType);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bussinessSubType = $this->BussinessSubTypes->newEntity();
        if ($this->request->is('post')) {
            $bussinessSubType = $this->BussinessSubTypes->patchEntity($bussinessSubType, $this->request->data);
            if ($this->BussinessSubTypes->save($bussinessSubType)) {
                $this->Flash->success(__('The bussiness sub type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bussiness sub type could not be saved. Please, try again.'));
            }
        }
        $bussinessTypes = $this->BussinessSubTypes->BussinessTypes->find('list');
        $this->set(compact('bussinessSubType', 'bussinessTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bussiness Sub Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bussinessSubType = $this->BussinessSubTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bussinessSubType = $this->BussinessSubTypes->patchEntity($bussinessSubType, $this->request->data);
            if ($this->BussinessSubTypes->save($bussinessSubType)) {
                $this->Flash->success(__('The bussiness sub type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bussiness sub type could not be saved. Please, try again.'));
            }
        }
        $bussinessTypes = $this->BussinessSubTypes->BussinessTypes->find('list');
        $this->set(compact('bussinessSubType', 'bussinessTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bussiness Sub Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bussinessSubType = $this->BussinessSubTypes->get($id);
        if ($this->BussinessSubTypes->delete($bussinessSubType)) {
            $this->Flash->success(__('The bussiness sub type has been deleted.'));
        } else {
            $this->Flash->error(__('The bussiness sub type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
