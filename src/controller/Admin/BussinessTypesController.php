<?php
namespace App\Controller\Admin;

use App\Controller\BackendController;

/**
 * BussinessTypes Controller
 *
 * @property \App\Model\Table\BussinessTypesTable $BussinessTypes
 */
class BussinessTypesController extends BackendController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('bussinessTypes', $this->paginate($this->BussinessTypes));
    }
    public function isAuthorized($user)
    {
        // The owner of an article can edit and delete it

        return true ;
    }

    /**
     * View method
     *
     * @param string|null $id Bussiness Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bussinessType = $this->BussinessTypes->get($id, [
            'contain' => ['BussinessSubTypes']
        ]);
        $this->set('bussinessType', $bussinessType);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bussinessType = $this->BussinessTypes->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (isset($data['image']) && $data['image']['error'] == 0) {
                $data['imagefile'] = $data['image'];
                $data['size'] = $data['image']['size'];
                $data['mime'] = $data['image']['type'];
            }
            unset($data['image']);
            $bussinessType = $this->BussinessTypes->patchEntity($bussinessType, $data);
            if ($this->BussinessTypes->save($bussinessType)) {
                $this->Flash->success(__('The bussiness type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bussiness type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bussinessType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bussiness Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bussinessType = $this->BussinessTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            if (isset($data['image']) && $data['image']['error'] == 0) {
                $data['imagefile'] = $data['image'];
                $data['size'] = $data['image']['size'];
                $data['mime'] = $data['image']['type'];
            }
            unset($data['image']);
            $bussinessType = $this->BussinessTypes->patchEntity($bussinessType, $data);
            if ($this->BussinessTypes->save($bussinessType)) {
                $this->Flash->success(__('The bussiness type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bussiness type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bussinessType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bussiness Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bussinessType = $this->BussinessTypes->get($id);
        if ($this->BussinessTypes->delete($bussinessType)) {
            $this->Flash->success(__('The bussiness type has been deleted.'));
        } else {
            $this->Flash->error(__('The bussiness type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
