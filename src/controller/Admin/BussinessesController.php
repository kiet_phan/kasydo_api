<?php
namespace App\Controller\Admin;

use App\Controller\BackendController;

/**
 * Bussinesses Controller
 *
 * @property \App\Model\Table\BussinessesTable $Bussinesses
 */
class BussinessesController extends BackendController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['BussinessSubTypes', 'Users']
        ];
        $this->set('bussinesses', $this->paginate($this->Bussinesses));
    }

    /**
     * View method
     *
     * @param string|null $id Bussiness id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bussiness = $this->Bussinesses->get($id, [
            'contain' => ['BussinessSubTypes', 'Users', 'Products',
              'Comments'
            ]
        ]);
        $this->set('bussiness', $bussiness);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bussiness = $this->Bussinesses->newEntity();
        if ($this->request->is('post')) {
            $bussiness = $this->Bussinesses->patchEntity($bussiness, $this->request->data);
            if ($this->Bussinesses->save($bussiness)) {
                $this->Flash->success(__('The bussiness has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bussiness could not be saved. Please, try again.'));
            }
        }
        $bussinessSubTypes = $this->Bussinesses->BussinessSubTypes->find('list');
        $users = $this->Bussinesses->Users->find('list');
        $this->set(compact('bussiness', 'bussinessSubTypes', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bussiness id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bussiness = $this->Bussinesses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bussiness = $this->Bussinesses->patchEntity($bussiness, $this->request->data);
            if ($this->Bussinesses->save($bussiness)) {
                $this->Flash->success(__('The bussiness has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bussiness could not be saved. Please, try again.'));
            }
        }
        $bussinessSubTypes = $this->Bussinesses->BussinessSubTypes->find('list');
        $users = $this->Bussinesses->Users->find('list');
        $this->set(compact('bussiness', 'bussinessSubTypes', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bussiness id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bussiness = $this->Bussinesses->get($id);
        if ($this->Bussinesses->delete($bussiness)) {
            $this->Flash->success(__('The bussiness has been deleted.'));
        } else {
            $this->Flash->error(__('The bussiness could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function change_status($id){
			$bussiness = $this->Bussinesses->get($id);
			$bussiness->status = 1 - $bussiness->status;
			$this->Bussinesses->save($bussiness);
			return $this->redirect(['action' => 'index']);

    }
    public function delete_all($ids = ''){

        if($this->request->query['ids']){
            $ids = $this->request->query['ids'];
            if(strlen($ids) > 0){
                $arr_ids = explode( ',', $ids );
                $this->Bussinesses->deleteAll(['id IN'=>$arr_ids]);
            }
        }
        return $this->redirect(['action' => 'index']);
    }
    public function isAuthorized($user)
    {
        // The owner of an article can edit and delete it

        return true ;
    }
}
