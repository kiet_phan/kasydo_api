<?php
namespace App\Controller\Admin;

use App\Controller\BackendController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends BackendController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('users', $this->paginate($this->Users));
    }
    public function push_notication(){
        $result =$this->Gcm->send(
          'kPr0Ic80PQs:APA91bFTkZ9a98V3acqMRlLa524GGoRU5lxt1alfd2PgbvBOW45dZaRO46M_i7PdHtlCSENDNR8369pHQKxFII-Bj4vKUwfxAkPLIQhJTp68dhSAkdz3cx2YkrRlYTiraVmFFr6Sh0tz',
          [
            'notification' => [
              'title' => 'Hello World',
              'body' => 'My awesome Hellow World!',
              'sound'         => 'default'
            ]
          ]
        );
        $response = $this->Gcm->response();
        echo '<pre>';
        print_r($response) ;die;

    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Addresses', 'Bussinesses']
        ]);
        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function delete_all($ids = ''){

      if($this->request->query['ids']){
				$ids = $this->request->query['ids'];
        if(strlen($ids) > 0){
	        $arr_ids = explode( ',', $ids );
	        $this->Users->deleteAll(['id IN'=>$arr_ids]);
        }
      }
	    return $this->redirect(['action' => 'index']);
    }
    public function isAuthorized($user)
    {
        // The owner of an article can edit and delete it

        return true ;
    }
}
