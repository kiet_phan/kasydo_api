<?php
namespace App\Controller\Admin;

use App\Controller\BackendController;
use Cake\ORM\TableRegistry;
use Doctrine\DBAL\Exception\TableExistsException;

/**
 * FeedBacks Controller
 *
 * @property \App\Model\Table\FeedBacksTable $FeedBacks
 */
class FeedBacksController extends BackendController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $feedBacks =TableRegistry::get('FeedBacks');
        $this->set('feedBacks', $this->paginate($feedBacks));
    }
    public function isAuthorized($user)
    {
        // The owner of an article can edit and delete it

        return true ;
    }

    /**
     * View method
     *
     * @param string|null $id Feed Back id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $feedBack = $this->FeedBacks->get($id, [
            'contain' => ['Users', 'Images']
        ]);
        $this->set('feedBack', $feedBack);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $feedBack = $this->FeedBacks->newEntity();
        if ($this->request->is('post')) {
            $feedBack = $this->FeedBacks->patchEntity($feedBack, $this->request->data);
            if ($this->FeedBacks->save($feedBack)) {
                $this->Flash->success(__('The feed back has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The feed back could not be saved. Please, try again.'));
            }
        }
        $users = $this->FeedBacks->Users->find('list');
        $this->set(compact('feedBack', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Feed Back id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $feedBack = $this->FeedBacks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $feedBack = $this->FeedBacks->patchEntity($feedBack, $this->request->data);
            if ($this->FeedBacks->save($feedBack)) {
                $this->Flash->success(__('The feed back has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The feed back could not be saved. Please, try again.'));
            }
        }
        $users = $this->FeedBacks->Users->find('list');
        $this->set(compact('feedBack', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Feed Back id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $feedBack = $this->FeedBacks->get($id);
        if ($this->FeedBacks->delete($feedBack)) {
            $this->Flash->success(__('The feed back has been deleted.'));
        } else {
            $this->Flash->error(__('The feed back could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
