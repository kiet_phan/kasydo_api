<?php
namespace App\Controller\Admin;

use App\Controller\BackendController;
use Cake\Event\Event;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 */
class AdminsController extends BackendController
{

    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
      parent::initialize();
      $this->Auth->allow('login');
    }
    public function beforeRender(Event $event) {
      parent::beforeRender($event);
      if($this->request->params['action'] == 'login')
        $this->viewBuilder()->layout('Admin/login');
     
    }
    public function index()
    {
        $this->set('admins', $this->paginate($this->Admins));
    }

    public function login(){
      if ($this->request->is('post')) {
        $user = $this->Auth->identify();
        if ($user) {
          $this->Auth->setUser($user);
          return $this->redirect($this->Auth->redirectUrl());
        } else {
          $this->Flash->error(
            __('Username or password is incorrect'), 'default', [], 'auth'
          );
        }
      }
    }

    /**
     * View method
     *
     * @param string|null $id Admin id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
  public function view($id = null)
  {
    $admin = $this->Admins->get($id, [
        'contain' => []
    ]);
    $this->set('admin', $admin);
  }
  public function logout(){
    return $this->redirect($this->Auth->logout());
  }


    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $admin = $this->Admins->newEntity();
        if ($this->request->is('post')) {
            $admin = $this->Admins->patchEntity($admin, $this->request->data);
            
//            var_dump($admin);die;
            if ($this->Admins->save($admin)) {
                $this->Flash->success(__('The admin has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The admin could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('admin'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Admin id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
  public function edit($id = null)
  {
      $admin = $this->Admins->get($id, [
          'contain' => []
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $admin = $this->Admins->patchEntity($admin, $this->request->data);
          if ($this->Admins->save($admin)) {
              $this->Flash->success(__('The admin has been saved.'));
              return $this->redirect(['action' => 'index']);
          } else {
              $this->Flash->error(__('The admin could not be saved. Please, try again.'));
          }
      }
      $this->set(compact('admin'));
  }

    /**
     * Delete method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
  public function delete($id = null)
  {
      $this->request->allowMethod(['post', 'delete']);
      $admin = $this->Admins->get($id);
      if ($this->Admins->delete($admin)) {
          $this->Flash->success(__('The admin has been deleted.'));
      } else {
          $this->Flash->error(__('The admin could not be deleted. Please, try again.'));
      }
      return $this->redirect(['action' => 'index']);
  }
  public function change_password($id){
    $admin = $this->Admins->get($id);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $admin = $this->Admins->patchEntity($admin, $this->request->data);
      if ($this->Admins->save($admin)) {
        $this->Flash->success(__('Password has bin change'));
        return $this->redirect(['action' => 'index']);
      } else {
        $this->Flash->error(__('The admin could not be saved. Please, try again.'));
      }
    }
  }
  public function isAuthorized($user)
  {
    // The owner of an article can edit and delete it
    if (in_array($this->request->action, ['view', 'login','logout'])) {
      return true;
    }
    if ($user['role'] == 'super_admin') {
      return true;
    }
    return false;
    return parent::isAuthorized($user);
  }
}
