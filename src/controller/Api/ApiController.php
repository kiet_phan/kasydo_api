<?php

/**
 * Created by PhpStorm.
 * User: TUANKIET
 * Date: 6/22/2015
 * Time: 12:36 AM
 */

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use \Firebase\JWT\JWT;
use \Firebase\JWT\SignatureInvalidException;
use Cake\Network\Exception\UnauthorizedException;
use JsonApi\Controller\JsonControllerTrait;
use JsonApi\Controller\JsonResponseTrait;

class ApiController extends AppController {

  use JsonControllerTrait;

use JsonResponseTrait;

  public $allow_action = [];
  private $currentUser = null;
  public $components = [
    'RequestHandler'
  ];

  public function initialize() {
    parent::initialize();
    $this->loadComponent('Paginator');
    
  }

  public function beforeFilter(\Cake\Event\Event $event) {
    parent::beforeFilter($event);
    $response = $this->check_token();
    if (!in_array($this->request->params["action"], $this->allow_action)) {
      if($response['is_logged']== false){
        $this ->responeData($response['message'],'false');
      }
    }
    // $this->check_logged();
  }

  private function check_token() {
    $token = $this->request->header('Access-token');
    $email = $this->request->header('uid');
    $users = TableRegistry::get('Users');
    $response =['is_logged'=>false,'message'> ''];
    $user = $users->find()->where(['email' => trim($email)])->first();

    if (!$user || empty($token)) {
      $response['message'] = 'Email Or Token is correct';
      $response['is_logged'] = false;
      return $response;
    }
    try {
      $decoded = JWT::decode($token, Security::salt(), array('HS256'));
      $exp = $decoded->exp;
      if (($user->id != $decoded->id) && $exp - time() <= 0) {
        $response['message'] = 'Invail Token';
        $response['is_logged'] = false;
        return $response;
      }
      $this->currentUser = $user;
      $response['message'] = 'OK';
      $response['is_logged'] = true;
      $response['data'] = $user;
      return $response;
    } catch (SignatureInvalidException $ex) {
      $response['message'] = 'Invail Token';
      $response['is_logged'] = false;
      return $response;
    }
    return $response;
  }

  public function getCurrentUser() {
    return $this->currentUser;
  }

  private function responeData($message = '', $success = 'false', $data = []) {
    $this->response->header([
      'Content-type: application/json'
    ]);
    $result['message'] = $message;
    $result['data'] = $data;
    $result['sussess'] = $success;
    $this->response->body(json_encode($result));
    $this->response->send();
    die;
  }

}
