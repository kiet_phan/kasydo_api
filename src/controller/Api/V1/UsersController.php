<?php

/**
 * Created by PhpStorm.
 * User: TUANKIET
 * Date: 6/22/2015
 * Time: 12:36 AM
 */

namespace App\Controller\Api\V1;

use ADmad\JwtAuth\Auth\JwtAuthenticate;
use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Utility\Security;
use \Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use WideImage\Exception\Exception;

class UsersController extends ApiController {

  public function initialize() {
    parent::initialize();
    $this->allow_action = ["login", 'add','forgotPassword','resetPasswordByToken'];
  }

  public function beforeFilter(Event $event) {
    parent::beforeFilter($event);

//		$this->Auth->allow(['add', 'token']);
  }

  public function index() {
    $this->set([
      'success' => true,
      'data' => "dada",
      '_serialize' => ['success', 'data'],
    ]);
  }

  public function login() {
    $result = [];
    $data = [];
    $success = false;
    $is_logged = false;
    $password = "";
    if (empty($this->request->data['password']))
      return $this->respondWithBadRequest('Password is not exist');
    if (empty($this->request->data['email']))
      return $this->respondWithBadRequest('Email is not exist');
    $password = $this->request->data['password'];
    $email = $this->request->data['email'];
    $users = TableRegistry::get('Users');
    $user = $users
      ->find()
      ->where(['email' => trim($email)])
      ->first();
    $is_logged = $user ? (new DefaultPasswordHasher)->check($password, $user->password) : $is_logged;
    if (!$is_logged) {
      $result['success'] = FALSE;
      $result['message'] = "invaild email or password";
      $result['_serialize'] = ['success', 'message'];
    } else {

      $data = [
        'token' => $token = JWT::encode([
          'id' => $user['id'],
          'exp' => TIME_OUT,
          ], Security::salt()),
      ];
      $user->is_login = true;
      $this->Users->save($user);
      $data['current_user'] = $user;
      $result['success'] = true;
      $result['data'] = $data;
      $result['_serialize'] = ['success', 'data'];
    }
    $this->set($result);
  }

  public function logOut() {
    $user = $this->getCurrentUser();
    $user->is_login = false;
    $this->Users->save($user);
    return $this->respondWithOK('success');
  }
  public function changePassword(){

  }
  public function updateAvatar(){
    $user = $this->getCurrentUser();
    if ($this->request->is('post')) {
      $data = $this->request->data;
      if (isset($data['image']) && $data['image']['error'] == 0) {
        $user->imagefile = $data['image'];
        $user->size = $data['image']['size'];
        $user->mime = $data['image']['type'];
      }
      unset($data);
      $user = $this->Users->save($user);
      if ( $this->Users->save($user)) {
        $user = $this->Users->get($user->id, [
          'contain' => ['Bussinesses']]);
        return $this->respondWithOK('success',[$user]);
      } else {
        return $this->respondWithBadRequest('failed');
      }
    }

  }

  public function add() {
    $user = $this->Users->newEntity();
    if (isset($this->request->data['birthday']))
      $this->request->data['birthday'] = new Time($this->request->data['birthday']);
    $this->request->data['is_login'] = true;
    if ($this->request->is('post')) {
      $user = $this->Users->patchEntity($user, $this->request->data);
      if ($this->Users->save($user)) {
        $token = JWT::encode(
            [
            'id' => $user->id,
            'exp' => TIME_OUT,
            ], Security::salt());
        return $this->respondWithOK("success", ['user' => $user, 'token' => $token]);
      } else {
        return $this->respondWithBadRequest("fail", $user->errors());
      }
    }
  }
  public function edit($id = null){
    $user= $this->getCurrentUser();
    if ($this->request->is(['patch', 'post', 'put'])) {
      if (isset($this->request->data['birthday']))
        $this->request->data['birthday'] = new Time($this->request->data['birthday']);
      $user = $this->Users->patchEntity($user, $this->request->data);
      if ($this->Users->save($user)) {
        return $this->respondWithOK('success');
      } else {
        $this->respondWithBadRequest('failed');
      }
    }

  }
  public function resetPasswordByToken(){
    if ($this->request->is('post')) {
      if(empty($this->request->data['email']) || empty($this->request->data['token_code']) || empty($this->request->data['password'])){
        return $this->respondWithBadRequest('failed');
      }
      if(empty($this->request->data['password']) != empty($this->request->data['confirm_password'])){
        return $this->respondWithBadRequest('invalid confirm_password');
      }
      $userEmail = $this->request->data['email'];
      $userObject = TableRegistry::get('Users');
      $user =$userObject
        ->find()
        ->where(['email' => trim($userEmail),'token' => $this->request->data['token_code']])
        ->first();
      if(empty($user)){
        return $this->respondWithBadRequest('invalid token');
      }
      $user->password = $this->request->data['password'];
      $user->token = null;
      if($userObject->save($user)){
        return $this->respondWithOK('success');
      }else{
        return $this->respondWithBadRequest('failed');
      }
    }
  }
  public function forgotPassword(){
    if(!$this->request->query('email')){
      return $this->respondWithBadRequest('failed');
    }
    $userObject = TableRegistry::get('Users');
    $userEmail = $this->request->query('email');

    $user =$userObject
      ->find()
      ->where(['email' => trim($userEmail)])
      ->first();
    if(empty($user)){
      return $this->respondWithBadRequest('failed');
    }
    $tokenCode = '';
    $tokenExists = true;
    while($tokenExists){
      $tokenCode = randomNumber(8);
      $userByToken = $userObject
        ->find()
        ->where(['token' =>$tokenCode])
        ->first();
      $tokenExists = empty($userByToken) ? false : true;
    }
    $user->token = $tokenCode;
    $userObject->save($user);

    $email = new Email('default');
    try{
      $email->from(['support@umblar.com' => 'Mã reset password'])
        ->to($userEmail)
        ->subject('Mã reset password')
        ->send('Vui lòng nhập mã code sau vào ứng dụng để reset password: '.$tokenCode);
      return $this->respondWithOK('success');
    }catch (Exception $e){
    }


  }

}
