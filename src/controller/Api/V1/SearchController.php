<?php
/**
 * Created by PhpStorm.
 * User: KIET
 * Date: 12/26/2015
 * Time: 1:09 AM
 */

namespace App\Controller\Api\V1;


use App\Controller\Api\ApiController;
use Cake\ORM\TableRegistry;

class SearchController extends ApiController
{
  public function initialize() {
    parent::initialize();
    $this->allow_action = ["index"];
  }
  public function index(){
    $current_user = $this->getCurrentUser();
    $current_user_id = empty($current_user) ? 0 : $current_user->id;
    $lat = empty($this->request->query('lat')) ? 0 : floatval($this->request->query('lat'));
    $long = empty($this->request->query('long')) ? 0 : floatval($this->request->query('long'));
    $conditions_bussiness = [];
    $conditions_products = [];
    if( $this->request->query('name')){
      $conditions_bussiness['LOWER(Bussinesses.name) LIKE'] = '%'.strtolower($this->request->query['name']).'%';
      $conditions_products['LOWER(Products.title) LIKE'] = '%'.strtolower($this->request->query['name']).'%';
    }
    if( $this->request->query('sub_type_id')){
      $conditions_bussiness['BussinessSubTypes.id'] = $this->request->query['sub_type_id'];
      $conditions_products['BussinessSubTypes.id'] = $this->request->query['sub_type_id'];
    }
    if( $this->request->query('type_id')){
      $conditions_bussiness['BussinessSubTypes.bussiness_type_id'] = $this->request->query['type_id'];
      $conditions_products['BussinessSubTypes.bussiness_type_id'] = $this->request->query['type_id'];
    }
    $conditions_products['Bussinesses.status'] = 1 ;
    $conditions_bussiness['Bussinesses.status'] = 1;


    $bussinessObject = TableRegistry::get('Bussinesses');
    $businesses = $bussinessObject->getNearBy($lat, $long,$conditions_bussiness,$current_user_id)->limit(20);
    $productObject = TableRegistry::get('Products');
    $products = $productObject->getProductByDistance($lat, $long,$conditions_products)->limit(20);
//    $businesses =unique_multidim_array($businesses,'id');
//    $products =unique_multidim_array($products,'id');
//    var_dump($businesses);die;
    foreach($businesses as $obj){
      $queryImage = TableRegistry::get('Images')->find()
        ->where(['foreign_key' => $obj->id,'model'=>'Bussinesses'])
        ->order(['created'=>'desc'])
        ->first();

      $obj->first_image = $queryImage;
    }
    foreach($products as $obj){
      $queryImage = TableRegistry::get('Images')->find()
        ->where(['foreign_key' => $obj->id,'model'=>'Products'])
        ->order(['created'=>'desc'])
        ->first();

      $obj->first_image = $queryImage;
    }
    return $this->respondWithOK('success',['businesses' =>$businesses->toArray(),'products'=>$products->toArray()]);
  }
  
}