<?php
namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;
use App\Controller\AppController;

/**
 * FeedBacks Controller
 *
 * @property \App\Model\Table\FeedBacksTable $FeedBacks
 */
class FeedBacksController extends ApiController
{

  /**
   * Index method
   *
   * @return void
   */
  public function index()
  {
    $this->paginate = [
      'contain' => ['Users']
    ];
    $this->set('feedBacks', $this->paginate($this->FeedBacks));
    $this->set('_serialize', ['feedBacks']);
  }

  /**
   * View method
   *
   * @param string|null $id Feed Back id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view($id = null)
  {
    $feedBack = $this->FeedBacks->get($id, [
      'contain' => ['Users']
    ]);
    $this->set('feedBack', $feedBack);
    $this->set('_serialize', ['feedBack']);
  }

  /**
   * Add method
   *
   * @return void Redirects on successful add, renders view otherwise.
   */
  public function add()
  {

    $feedBack = $this->FeedBacks->newEntity();
    if ($this->request->is('post')) {
      $data = $this->request->data;
      $current_user = $this->getCurrentUser();
      if (isset($data['images'])) {

        $data['image_list'] = $data['images'];
        unset($data['images']);
      }
      $feedBack = $this->FeedBacks->patchEntity($feedBack, $data);
      $feedBack->user_id = $current_user->id;
      if ($this->FeedBacks->save($feedBack)) {
        return $this->respondWithOK('success');
      } else {
        return $this->respondWithBadRequest('failed', [$feedBack->errors()]);
      }
    }

  }

  /**
   * Edit method
   *
   * @param string|null $id Feed Back id.
   * @return void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null)
  {
    $feedBack = $this->FeedBacks->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $feedBack = $this->FeedBacks->patchEntity($feedBack, $this->request->data);
      if ($this->FeedBacks->save($feedBack)) {
        $this->Flash->success(__('The feed back has been saved.'));
        return $this->redirect(['action' => 'index']);
      } else {
        $this->Flash->error(__('The feed back could not be saved. Please, try again.'));
      }
    }
    $users = $this->FeedBacks->Users->find('list', ['limit' => 200]);
    $this->set(compact('feedBack', 'users'));
    $this->set('_serialize', ['feedBack']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Feed Back id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function delete($id = null)
  {
    $this->request->allowMethod(['post', 'delete']);
    $feedBack = $this->FeedBacks->get($id);
    if ($this->FeedBacks->delete($feedBack)) {
      $this->Flash->success(__('The feed back has been deleted.'));
    } else {
      $this->Flash->error(__('The feed back could not be deleted. Please, try again.'));
    }
    return $this->redirect(['action' => 'index']);
  }
}
