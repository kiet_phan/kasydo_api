<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends ApiController
{

  /**
   * Index method
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->allow_action = ['view', 'index'];

  }

  public function index()
  {
    $current_user_id = 0;
    if($current_user = $this->getCurrentUser()){
      $current_user_id = $current_user->id;
    }
    $conditions = [];

    if (isset($this->request->params['bussiness_id'])) {
      $conditions['Products.bussiness_id'] = $this->request->params['bussiness_id'];
    }
    $containTable = [
      'Bussinesses',
      'FirstImage'=> [
        'queryBuilder' => function ($q) {
          return $q->order(['FirstImage.created' =>'DESC']);
        }
      ],
      'TopComments' => [
        'queryBuilder' => function ($q) {
          return $q->order(['TopComments.created_at' =>'DESC']);
        },
        'Users' => [
          'queryBuilder' => function ($q) {
            return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
          }
        ],
      ],
      'UserRanks'=> function ($q) {
        return $q
          ->select(['relation_id',
            'total_user_rate' => $q->func()->count('user_id'),
            'total_rank'=>$q->func()->sum('rank_value')
          ])
          ->group('relation_id');
      },
      'Users'=>function ($q) use($current_user_id) {
        return $q->where(['Users.id'=>$current_user_id]);
      },
      'UserRates'=>function ($q) use($current_user_id) {
        return $q->where(['UserRates.id'=>$current_user_id]);
      }
    ];

    $this->paginate = [
      'limit' => 20
    ];
    $productObject = TableRegistry::get('Products');
    $query = $productObject->find('all',[
      'conditions' => $conditions,
      'contain' => $containTable
    ]);

    $products = $this->paginate($query)->toArray();
    $pagination = $this->Paginator->request->params['paging']['Products'];
    foreach($products as  $product){
      $count = TableRegistry::get('Comments')->find()->where(['Comments.commentable_type' => 'Products','Comments.commentable_id' =>$product->id])
        ->count();
      $product->count_comment =$count;
    }
//    var_dump($products);die;
    return $this->respondWithOK('success', ['products' => $products, 'pagination' => $pagination]);
  }
  

  /**
   * View method
   *
   * @param string|null $id Product id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view($id = null)
  {
    $current_user_id = 0;
    if($current_user = $this->getCurrentUser()){
      $current_user_id = $current_user->id;
    }
    $containTable = [
      'Bussinesses',
      'TopComments' => [
        'queryBuilder' => function ($q) {
          return $q->order(['TopComments.created_at' =>'DESC']);
        },
        'Users' => [
          'queryBuilder' => function ($q) {
            return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
          }
        ],
      ],
      'Images',
      'UserRanks'=> function ($q) {
        return $q
          ->select(['relation_id',
            'total_user_rate' => $q->func()->count('user_id'),
            'total_rank'=>$q->func()->sum('rank_value')
          ])
          ->group('relation_id');
      },
      'Users'=>function ($q) use($current_user_id) {
        return $q->where(['Users.id'=>$current_user_id]);
      },
      'UserRates'=>function ($q) use($current_user_id) {
        return $q->where(['UserRates.id'=>$current_user_id]);
      }
    ];
    $product = $this->Products->get($id, [
      'contain' => $containTable
    ]);
    return $this->respondWithOK("success", [$product]);
  }

  /**
   * Add method
   *
   * @return void Redirects on successful add, renders view otherwise.
   */
  public function add()
  {
    $product = $this->Products->newEntity();
    if ($this->request->is('post')) {
      $data = [];
      $data = $this->request->data;

      if (isset($data['images'])) {
        
        $data['image_list'] = json_decode($data['images']);
        unset($data['images']);
      }
      $product = $this->Products->patchEntity($product, $data);
      if ($this->Products->save($product)) {
        return $this->respondWithOK("success", [$product]);
      } else {
        $this->Flash->error(__('The product could not be saved. Please, try again.'));
        return $this->respondWithBadRequest("failed", $product->errors());
      }
    }
  }

  /**
   * Edit method
   *
   * @param string|null $id Product id.
   * @return void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null)
  {
    if(empty($this->request->params['bussiness_id'])){
      return $this->respondWithBadRequest("failed");
    }
    $current_user= $this->getCurrentUser();
    $bussiness = TableRegistry::get('Bussinesses')->find('all')
    ->where(['id' => $this->request->params['bussiness_id'],'user_id'=> $current_user->id])
      ->contain('Products')
    ->first();

    if(empty($bussiness)){
      return $this->respondWithBadRequest("failed");
    }
    $product = $this->Products->find('all')
      ->where(['id' => $id,'bussiness_id' => $bussiness->id])->first();
    if ($this->request->is(['patch', 'post', 'put'])) {
      $data = [];
      $data = $this->request->data;

      if (isset($data['images'])) {

        $data['image_list'] = json_decode($data['images']);
        unset($data['images']);
      }
      $product = $this->Products->patchEntity($product, $data);
      if ($this->Products->save($product)) {
        return $this->respondWithOK("success", [$product]);
      } else {
        return $this->respondWithBadRequest("failed");
      }
    }
  }

  /**
   * Delete method
   *
   * @param string|null $id Product id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function delete($id = null)
  {
    $this->request->allowMethod(['post', 'delete']);
    if(empty($this->request->params['bussiness_id'])){
      return $this->respondWithBadRequest("failed");
    }
    $current_user= $this->getCurrentUser();
    $bussiness = TableRegistry::get('Bussinesses')->find('all')
      ->where(['id' => $this->request->params['bussiness_id'],'user_id'=> $current_user->id])
      ->contain('Products')
      ->first();

    if(empty($bussiness)){
      return $this->respondWithBadRequest("failed");
    }
    $product = $this->Products->find('all')
      ->where(['id' => $id,'bussiness_id' => $bussiness->id])->first();

    if ($this->Products->delete($product)) {
      return $this->respondWithOK("success");
    } else {
      return $this->respondWithBadRequest("failed");
    }
  }

  public function rating($id = null){
    $user =$this->getCurrentUser();
    if(!$id || empty(intval($this->request->query('rank_value'))) ){
      return $this->respondWithBadRequest("failed");
    }
    $product = $this->Products->get($id,['contain'=>['UserRanks']]);
    $userRanksTable = TableRegistry::get('UserRanks');
//    var_dump($product);die;
    $userRank = $userRanksTable->find()->where(['user_id' => $user->id, 'relation_id' => $product->id,'relation_type'=>'Products' ])->first();
    $userRankId = $userRank ? $userRank->id : null;
    $data = [
      'user_ranks'=>[
        [
          'id' => $userRankId,
          'user_id'=>$user->id,
          'relation_type'=>'Products',
          'rank_value' => $this->request->query('rank_value')
        ]
      ]
    ];
    $product = $this->Products->patchEntity($product, $data, [
      'associated' => ['UserRanks']
    ]);
    if( $this->Products->save($product)){
      return $this->respondWithOK("success");
    }else{
      return $this->respondWithBadRequest("failed", $product->errors());
    }
  }

}
