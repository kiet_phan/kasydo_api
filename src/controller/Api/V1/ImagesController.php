<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 */
class ImagesController extends ApiController {

  /**
   * Index method
   *
   * @return void
   */
  public function initialize() {
    parent::initialize();
    $this->allow_action = ["login", 'add'];
  }

  public function index() {
    $this->paginate = [
      'contain' => ['Products']
    ];
    $this->set('images', $this->paginate($this->Images));
    $this->set('_serialize', ['images']);
  }

  /**
   * View method
   *
   * @param string|null $id Image id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view($id = null) {
    $image = $this->Images->get($id, [
      'contain' => ['Products']
    ]);
    $this->set('image', $image);
    $this->set('_serialize', ['image']);
  }

  /**
   * Add method
   *
   * @return void Redirects on successful add, renders view otherwise.
   */
  public function add() {
    $image = $this->Images->newEntity();
    if ($this->request->is('post')) {
      $data = [];
      if(!empty($this->request->params['bussiness_id'])){
        $data['foreign_key'] = $this->request->params['bussiness_id'];
        $data['model'] ='Bussinesses';
      }
      if (isset($this->request->data['image']) && $this->request->data['image']['error'] == 0) {
        $data['mime'] = $this->request->data['image']['type'];
        $data['size'] = $this->request->data['image']['size'];
        $data['imagefile'] = $this->request->data['image'];
        $image = $this->Images->patchEntity($image, $data);
        if ($this->Images->save($image)) {
          return $this->respondWithOK("success", [$this->Images->get($image->id)]);
        } else {
          return $this->respondWithBadRequest("failed", $image->errors());
        }
      }

    }
    return $this->respondWithBadRequest("failed");
  }

  /**
   * Edit method
   *
   * @param string|null $id Image id.
   * @return void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {
    $image = $this->Images->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $image = $this->Images->patchEntity($image, $this->request->data);
      if ($this->Images->save($image)) {
        $this->Flash->success(__('The image has been saved.'));
        return $this->redirect(['action' => 'index']);
      } else {
        $this->Flash->error(__('The image could not be saved. Please, try again.'));
      }
    }
    $products = $this->Images->Products->find('list', ['limit' => 200]);
    $this->set(compact('image', 'products'));
    $this->set('_serialize', ['image']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Image id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function delete($id = null) {
    $this->request->allowMethod(['post', 'delete']);
    $image = $this->Images->get($id);
    if ($this->Images->delete($image)) {
      return $this->respondWithOK("success");
    } else {
      return $this->respondWithBadRequest("fail");
    }
  }

}
