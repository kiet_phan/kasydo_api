<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;

/**
 * BussinessSubTypes Controller
 *
 * @property \App\Model\Table\BussinessSubTypesTable $BussinessSubTypes
 */
class BussinessSubTypesController extends ApiController {

  /**
   * Index method
   *
   * @return void
   */
  public function initialize() {
    parent::initialize();
    $this->allow_action = ["index"];
  }
  public function index() {
    $conditions = [];
    if(!empty( $this->request->params['bussiness_type_id'])) $conditions["bussiness_type_id"] =  $this->request->params['bussiness_type_id']; 
    $this->paginate = [
      'contain' => ['BussinessTypes'],
      'conditions' => $conditions
    ];
    $this->set('bussinessSubTypes', $this->paginate($this->BussinessSubTypes));
    $this->set('_serialize', ['bussinessSubTypes']);
  }

  /**
   * View method
   *
   * @param string|null $id Bussiness Sub Type id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view($id = null) {
    $bussinessSubType = $this->BussinessSubTypes->get($id, [
      'contain' => ['BussinessTypes', 'Bussinesses']
    ]);
    $this->set('bussinessSubType', $bussinessSubType);
    $this->set('_serialize', ['bussinessSubType']);
  }

  /**
   * Add method
   *
   * @return void Redirects on successful add, renders view otherwise.
   */
  public function add() {
    $bussinessSubType = $this->BussinessSubTypes->newEntity();
    if ($this->request->is('post')) {
      $bussinessSubType = $this->BussinessSubTypes->patchEntity($bussinessSubType, $this->request->data);
      if ($this->BussinessSubTypes->save($bussinessSubType)) {
        $this->Flash->success(__('The bussiness sub type has been saved.'));
        return $this->redirect(['action' => 'index']);
      } else {
        $this->Flash->error(__('The bussiness sub type could not be saved. Please, try again.'));
      }
    }
    $bussinessTypes = $this->BussinessSubTypes->BussinessTypes->find('list', ['limit' => 200]);
    $this->set(compact('bussinessSubType', 'bussinessTypes'));
    $this->set('_serialize', ['bussinessSubType']);
  }

  /**
   * Edit method
   *
   * @param string|null $id Bussiness Sub Type id.
   * @return void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {
    $bussinessSubType = $this->BussinessSubTypes->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $bussinessSubType = $this->BussinessSubTypes->patchEntity($bussinessSubType, $this->request->data);
      if ($this->BussinessSubTypes->save($bussinessSubType)) {
        $this->Flash->success(__('The bussiness sub type has been saved.'));
        return $this->redirect(['action' => 'index']);
      } else {
        $this->Flash->error(__('The bussiness sub type could not be saved. Please, try again.'));
      }
    }
    $bussinessTypes = $this->BussinessSubTypes->BussinessTypes->find('list', ['limit' => 200]);
    $this->set(compact('bussinessSubType', 'bussinessTypes'));
    $this->set('_serialize', ['bussinessSubType']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Bussiness Sub Type id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function delete($id = null) {
    $this->request->allowMethod(['post', 'delete']);
    $bussinessSubType = $this->BussinessSubTypes->get($id);
    if ($this->BussinessSubTypes->delete($bussinessSubType)) {
      $this->Flash->success(__('The bussiness sub type has been deleted.'));
    } else {
      $this->Flash->error(__('The bussiness sub type could not be deleted. Please, try again.'));
    }
    return $this->redirect(['action' => 'index']);
  }

}
