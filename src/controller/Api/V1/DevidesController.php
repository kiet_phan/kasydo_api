<?php
namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;

/**
 * Devides Controller
 *
 * @property \App\Model\Table\DevidesTable $Devides
 */
class DevidesController extends ApiController
{

    /**
     * Index method
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->allow_action = ["add",'view'];

    }
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ds']
        ];
        $this->set('devides', $this->paginate($this->Devides));
        $this->set('_serialize', ['devides']);
    }

    /**
     * View method
     *
     * @param string|null $id Devide id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $devide = $this->Devides->get($id);
        $this->set('devide', $devide);
        $this->set('_serialize', ['devide']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $devide = $this->Devides->newEntity();
        if ($this->request->is('post')) {
            $devide = $this->Devides->patchEntity($devide, $this->request->data);
            if ($this->Devides->save($devide)) {
                return $this->respondWithOK("success", [$devide]);
            } else {
                return $this->respondWithOK("success", [$devide]);
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Devide id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $devide = $this->Devides->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $devide = $this->Devides->patchEntity($devide, $this->request->data);
            if ($this->Devides->save($devide)) {
                return $this->respondWithOK("success", [$devide]);
            } else {
                return $this->respondWithBadRequest("fail", $devide->errors());
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Devide id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $devide = $this->Devides->get($id);
        if ($this->Devides->delete($devide)) {
            $this->Flash->success(__('The devide has been deleted.'));
        } else {
            $this->Flash->error(__('The devide could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
