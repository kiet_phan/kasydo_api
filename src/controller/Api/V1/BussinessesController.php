<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;
use Cake\ORM\TableRegistry;

/**
 * Bussinesses Controller
 *
 * @property \App\Model\Table\BussinessesTable $Bussinesses
 */
class BussinessesController extends ApiController
{

  /**
   * Index method
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->allow_action = ["view"];

  }

  public function index()
  {
    $current_user = $this->getCurrentUser();
    $current_user_id =$current_user->id;
    $containTable =[
      'BussinessSubTypes',
      'TopComments' => [
        'queryBuilder' => function ($q) {
          return $q->order(['TopComments.created_at' =>'DESC']);
        },
        'Users' => [
          'queryBuilder' => function ($q) {
            return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
          }
        ]
      ],
      'FirstImage' => [
        'queryBuilder' => function ($q) {
          return $q->order(['FirstImage.created' =>'DESC']);
        }
      ],
      'UserRanks'=> function ($q) {
        return $q
          ->select(['relation_id',
            'total_user_rate' => $q->func()->count('user_id'),
            'total_rank'=>$q->func()->sum('rank_value')
          ])
          ->group('relation_id');
      },
      'UserRates'=>function ($q) use($current_user_id) {
        return $q->where(['UserRates.id'=>$current_user_id]);
      }
    ];
    $conditions = [
      'Bussinesses.user_id' => $current_user->id,
      'Bussinesses.status' => 1,
    ];
    $this->paginate = [
      'contain' =>$containTable,
      'conditions' => $conditions,
      'limit' => 20
    ];


    $bussinessObject = TableRegistry::get('Bussinesses');
    $query = $bussinessObject->find('all');

    $bussinesses = $this->paginate($query);

    $pagination = $this->Paginator->request->params['paging']['Bussinesses'];
    $this->set('pagination', $pagination);
    $this->set('bussinesses', $bussinesses);
    $this->set('_serialize', ['bussinesses', 'pagination']);
  }

  /**
   * View method
   *
   * @param string|null $id Bussiness id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view($id = null)
  {
    $current_user = $this->getCurrentUser();
    $current_user_id = empty($current_user) ? 0 : $current_user->id;
    $bussiness = $this->Bussinesses->get($id, [
      'contain' => ['BussinessSubTypes', 'Users',
        'Comments'=> [
          'Users' => [
            'queryBuilder' => function ($q) {
              return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
            }
          ]
        ],
        'Images',
        'UserRanks'=> function ($q) {
          return $q
            ->select(['relation_id',
              'total_user_rate' => $q->func()->count('user_id'),
              'total_rank'=>$q->func()->sum('rank_value')
            ])
            ->group('relation_id');
        },
        'UserRates'=>function ($q) use($current_user_id) {
          return $q->where(['UserRates.id'=>$current_user_id]);
        }
      ]
    ])->toArray();

    $productObject = TableRegistry::get('Products');
    $contain = [
      'Images' => [
        'queryBuilder' => function ($q) {
          return $q->limit(1);
        }
      ],
      'TopComments' => [
        'queryBuilder' => function ($q) {
          return $q->order(['TopComments.created_at' =>'DESC']);
        },
        'Users' => [
          'queryBuilder' => function ($q) {
            return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
          }
        ]
      ],
      'UserRanks'=> function ($q) {
        return $q
          ->select(['relation_id',
            'total_user_rate' => $q->func()->count('user_id'),
            'total_rank'=>$q->func()->sum('rank_value')
          ])
          ->group('relation_id');
      },
      'Users'=>function ($q) use($current_user_id) {
        return $q->where(['Users.id'=>$current_user_id]);
      }
    ];
    $products = $productObject->find('all', [
      'conditions' => ['bussiness_id' => $id],
      'contain' => $contain
    ])->limit(2);
    $bussiness['products'] = $products;
    $this->set('bussiness', $bussiness);
    $this->set('_serialize', ['bussiness']);
  }

  /**
   * Add method
   *
   * @return void Redirects on successful add, renders view otherwise.
   */
  public function add()
  {
    $current_user = $this->getCurrentUser();
//    $bussiness_exists = $this->Bussinesses->find()->where(['user_id' => $current_user->id])->first();
//    if($bussiness_exists){
//      return $this->respondWithOK('failed',['error'=>'bussiness is exist']);
//    }
    $bussiness = $this->Bussinesses->newEntity();
    $bussiness->user_id = $current_user->id;
    if ($this->request->is('post')) {
      $data = $this->request->data;
      if (isset($data['image']) && $data['image']['error'] == 0) {
        $data['imagefile'] = $data['image'];
        $data['size'] = $data['image']['size'];
        $data['mime'] = $data['image']['type'];
      }
//      foreach ($data['image'] as $key => $value) {
//        $data['images'][] = empty($value['error']) ?  ['model' => $this->Bussinesses->alias(),'imagefile' => $value,'mime' =>$value['type'],'size' => $value['size'] ] : null;
//      }
      unset($data['image']);
//      $bussiness = $this->Bussinesses->patchEntity($bussiness, $data,['associated' => ['Images']]);
      $bussiness = $this->Bussinesses->patchEntity($bussiness, $data, ['associated' => ['Images']]);
      if ($this->Bussinesses->save($bussiness)) {
        return $this->respondWithOK("success", [$bussiness]);
      } else {
        return $this->respondWithBadRequest("fail", $bussiness->errors());
      }
    }
    return $this->respondWithOK("success");
  }

  /**
   * Edit method
   *
   * @param string|null $id Bussiness id.
   * @return void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null)
  {
    $bussiness = $this->Bussinesses->get($id);
    if ($this->request->is(['patch', 'post', 'put'])) {
//      var_dump($putfp = fopen('php://input', 'r'));die;
      $data = $this->request->data;
      if (isset($data['image']) && $data['image']['error'] == 0) {
        $data['imagefile'] = $data['image'];
        $data['size'] = $data['image']['size'];
        $data['mime'] = $data['image']['type'];
      }
      unset($data['image']);

      $bussiness = $this->Bussinesses->patchEntity($bussiness, $data);
      if ($this->Bussinesses->save($bussiness)) {
        $bussiness = $this->Bussinesses->get($id);
        return $this->respondWithOK("success", [$bussiness]);
      } else {
        return $this->respondWithBadRequest("fail", [$bussiness->errors()]);
      }
    }
  }

  /**
   * Delete method
   *
   * @param string|null $id Bussiness id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function delete($id = null)
  {

    $current_user= $this->getCurrentUser();

    $this->request->allowMethod(['delete']);
    $bussiness = $this->Bussinesses->find()
      ->where(['id' => $id,'user_id'=> $current_user->id])
    ->first();
    if(empty($bussiness)){
      return $this->respondWithBadRequest("failed");
    }
    if ($this->Bussinesses->delete($bussiness)) {
      return $this->respondWithOK("success");
    } else {
      return $this->respondWithBadRequest("failed");
    }
  }
  public function rating($id = null){
    $user =$this->getCurrentUser();
    if(!$id || empty(intval($this->request->query('rank_value'))) ){
      return $this->respondWithBadRequest("failed");
    }
    $business = $this->Bussinesses->get($id,['contain'=>['UserRanks']]);
    $userRanksTable = TableRegistry::get('UserRanks');
//    var_dump($product);die;
    $userRank = $userRanksTable->find()->where(['user_id' => $user->id, 'relation_id' => $business->id,'relation_type'=>'Bussinesses' ])->first();
    $userRankId = $userRank ? $userRank->id : null;
    $data = [
      'user_ranks'=>[
        [
          'id' => $userRankId,
          'user_id'=>$user->id,
          'relation_type'=>'Bussinesses',
          'rank_value' => $this->request->query('rank_value')
        ]
      ]
    ];
    $business = $this->Bussinesses->patchEntity($business, $data, [
      'associated' => ['UserRanks']
    ]);
    if( $this->Bussinesses->save($business)){
      return $this->respondWithOK("success");
    }else{
      return $this->respondWithBadRequest("failed", $business->errors());
    }
  }

}
