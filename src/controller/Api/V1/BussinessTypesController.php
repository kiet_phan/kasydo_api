<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Controller\Api\ApiController;

/**
 * BussinessTypes Controller
 *
 * @property \App\Model\Table\BussinessTypesTable $BussinessTypes
 */
class BussinessTypesController extends ApiController {

  /**
   * Index method
   *
   * @return void
   */
  public function initialize() {
    parent::initialize();
    $this->allow_action = ["index"];
  }

  public function index() {
    $this->set('bussinessTypes', $this->paginate($this->BussinessTypes));
    $this->set('_serialize', ['bussinessTypes']);
  }

  /**
   * View method
   *
   * @param string|null $id Bussiness Type id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view($id = null) {
    $bussinessType = $this->BussinessTypes->get($id, [
      'contain' => ['BussinessSubTypes']
    ]);
    $this->set('bussinessType', $bussinessType);
    $this->set('_serialize', ['bussinessType']);
  }

  /**
   * Add method
   *
   * @return void Redirects on successful add, renders view otherwise.
   */
  public function add() {
    $bussinessType = $this->BussinessTypes->newEntity();
    if ($this->request->is('post')) {
      $bussinessType = $this->BussinessTypes->patchEntity($bussinessType, $this->request->data);
      if ($this->BussinessTypes->save($bussinessType)) {
        return $this->respondWithOK("success", [$bussinessType]);
      } else {
        return $this->respondWithBadRequest("fail", $bussinessType->errors());
      }
    }
    $this->set(compact('bussinessType'));
    $this->set('_serialize', ['bussinessType']);
  }

  /**
   * Edit method
   *
   * @param string|null $id Bussiness Type id.
   * @return void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function edit($id = null) {
    $bussinessType = $this->BussinessTypes->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $bussinessType = $this->BussinessTypes->patchEntity($bussinessType, $this->request->data);
      if ($this->BussinessTypes->save($bussinessType)) {
        $this->Flash->success(__('The bussiness type has been saved.'));
        return $this->redirect(['action' => 'index']);
      } else {
        $this->Flash->error(__('The bussiness type could not be saved. Please, try again.'));
      }
    }
    $this->set(compact('bussinessType'));
    $this->set('_serialize', ['bussinessType']);
  }

  /**
   * Delete method
   *
   * @param string|null $id Bussiness Type id.
   * @return \Cake\Network\Response|null Redirects to index.
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function delete($id = null) {
    $this->request->allowMethod(['post', 'delete']);
    $bussinessType = $this->BussinessTypes->get($id);
    if ($this->BussinessTypes->delete($bussinessType)) {
      $this->Flash->success(__('The bussiness type has been deleted.'));
    } else {
      $this->Flash->error(__('The bussiness type could not be deleted. Please, try again.'));
    }
    return $this->redirect(['action' => 'index']);
  }

}
