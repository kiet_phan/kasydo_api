<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HomeController
 *
 * @author KIET
 */

namespace App\Controller\Api\V1;
use Cake\ORM\TableRegistry;
use App\Controller\Api\ApiController;

class HomeController extends ApiController {

  //put your code here
  public function initialize() {
    parent::initialize();
    $this->allow_action = ["index"];
  }
  public function index(){
    $current_user = $this->getCurrentUser();
    $current_user_id = empty($current_user) ? 0 : $current_user->id;
    $containTable =[
      'BussinessSubTypes',
      'Comments' => [
        'queryBuilder' => function ($q) {
          return $q->order(['Comments.created_at' =>'DESC'])
            ->limit(2);
        },
        'Users' => [
          'queryBuilder' => function ($q) {
            return $q->select(['id','first_name','last_name','profile_image'])->where("1=1");
          }
        ]
      ],
      'Images' => [
        'queryBuilder' => function ($q) {
          return $q->limit(1);
        }
      ],
      'UserRanks'=> function ($q) {
        return $q
          ->select(['relation_id',
            'total_user_rate' => $q->func()->count('user_id'),
            'total_rank'=>$q->func()->sum('rank_value')
          ])
          ->group('relation_id');
      },
      'Users'=>function ($q) use($current_user_id) {
        return $q->where(['Users.id'=>$current_user_id]);
      }
    ];
    $bussinesses = TableRegistry::get('bussinesses');
    $lat = empty(floatval($this->request->query('lat'))) ? 0 : $this->request->query('lat');
    $long = empty(floatval($this->request->query('long'))) ? 0 : $this->request->query('long');
    $result = $bussinesses->getNearBy($lat,$long,['Users.is_login !='=> '0','bussinesses.status' => true],$current_user_id);
    foreach($result as $obj){
      $queryImage = TableRegistry::get('Images')->find()
        ->where(['foreign_key' => $obj->id,'model'=>'Bussinesses'])
        ->order(['created'=>'desc'])
        ->first();

      $obj->first_image = $queryImage;
    }
//    $result =unique_multidim_array($result,'id');


    return $this->respondWithOK("success",$result->toArray());
  }

}
