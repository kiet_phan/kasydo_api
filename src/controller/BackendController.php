<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BackendController
 *
 * @author TUANKIET
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class BackendController extends AppController {

  //put your code here
  public $helpers = [
    'Less.Less', // required for parsing less files
    'BootstrapUI.Form',
    'BootstrapUI.Html',
    'BootstrapUI.Flash',
    'BootstrapUI.Paginator'
  ];

  public function beforeRender(Event $event) {
    parent::beforeRender($event);
    $this->set(['userData'=> $this->Auth->user()]);
    $this->viewBuilder()->layout('Admin/layout_default');
  }

//    public $helpers = [
//        'Html' => [
//            'className' => 'Bootstrap3.BootstrapHtml'
//        ],
//        'Form' => [
//            'className' => 'Bootstrap3.BootstrapForm',
//            'useCustomFileInput' => true
//        ],
//        'Paginator' => [
//            'className' => 'Bootstrap3.BootstrapPaginator'
//        ],
//        'Modal' => [
//            'className' => 'Bootstrap3.BootstrapModal'
//        ]
//    ];

  public function initialize() {
    parent::initialize();

    $this->loadComponent('Flash'); // Include the FlashComponent
    $this->loadComponent('Auth', [
      'loginAction' => [
          'controller' => 'admins',
          'action' => 'login'
      ],
      'loginRedirect' => [
          'controller' => 'bussinesses',
          'action' => 'index'
      ],
      'logoutRedirect' => [
          'controller' => 'admins',
          'action' => 'login'
      ],
      'authenticate' => ['Form' => [
              'fields' => ['username' => 'email', 'password' => 'password'],
              'userModel' => 'Admins'
          ]
      ],
      'authorize' => ['Controller'],

    ]);
    $this->Auth->sessionKey ="Auth.admin";
    $this->loadComponent('CakeGcm.Gcm', [
      'api' => [
        'key' => GOOGLE_API_KEY
      ]
    ]);
  }

}
