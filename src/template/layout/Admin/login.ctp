<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <?php echo $this->Html->charset(); ?>
        <title>
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('../admin/css/bootstrap.min.css') ?>
        <?= $this->Html->css('../admin/css/style.css') ?>
        <?= $this->Html->css('../font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->script('../admin/bower_components/jquery/dist/jquery.min.js') ?>
        <?= $this->Html->script('../admin/js/bootbox.min.js') ?>
        <?= $this->Html->script('../admin/js/script.js') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <?= $this->fetch('content') ?>
                  
                </div>
            </div>
        </div>

        <?= $this->Html->script('../admin/js/bootstrap.min.js') ?>



    </body>

</html>
