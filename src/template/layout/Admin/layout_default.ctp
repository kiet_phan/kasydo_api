<!DOCTYPE html>
<html lang="en">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $this->fetch('title') ?>
        </title>
        <!-- Bootstrap Core CSS -->
        <?= $this->Html->meta('icon') ?>
        <?= $this->Html->css('../admin/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('../admin/bower_components/metisMenu/dist/metisMenu.min.css') ?>
        <?= $this->Html->css('../admin/dist/css/timeline.css') ?>
        <?= $this->Html->css('../admin/dist/css/sb-admin-2.css') ?>
        <?= $this->Html->css('../admin/bower_components/morrisjs/morris.css') ?>

        <?= $this->Html->css('../admin/bower_components/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('../admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') ?>
        <?= $this->Html->css('../admin/bower_components/datatables-responsive/css/dataTables.responsive.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

        <?= $this->Html->script('../admin/bower_components/jquery/dist/jquery.min.js') ?>
        <?= $this->Html->script('../admin/js/bootbox.min.js') ?>
        <?= $this->Html->script('../admin/js/admin_script.js') ?>
        <?= $this->Html->script('../admin/js/script.js') ?>
        <?= $this->Html->script('ckeditor/ckeditor.js') ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            var path_url = "<?= $this->request->host().$this->request->base ?>";
        </script>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Umbalar App</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">


                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'admins', 'action' => 'view',$userData['id']] );?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
<!--                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>-->
<!--                            </li>-->
                            <li class="divider"></li>
                            <li><a href="/admin/admins/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

               <?= $this->element("Admin/right_menu") ?>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <?= $this->fetch('content') ?>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>

        <!-- Bootstrap Core JavaScript -->
        <?= $this->Html->script('../admin/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>
        <!-- Metis Menu Plugin JavaScript -->
        <?= $this->Html->script('../admin/bower_components/metisMenu/dist/metisMenu.min.js') ?>
        <?= $this->Html->script('../admin/bower_components/raphael/raphael-min.js') ?>
        <?//= $this->Html->script('../admin/bower_components/morrisjs/morris.min.js') ?>
        <?//= $this->Html->script('../admin/js/morris-data.js') ?>
        <?= $this->Html->script('../admin/dist/js/sb-admin-2.js') ?>

    </body>

</html>
