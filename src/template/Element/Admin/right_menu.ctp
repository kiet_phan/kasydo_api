
 <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'admins', 'action' => 'index'] );?>"><i class="fa fa-dashboard fa-fw"></i> Admin</a>
                            </li>
                             <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'bussinesses', 'action' => 'index'] );?>"><i class="fa fa-dashboard fa-fw"></i> Bussinesses</a>
                            </li>
                            <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'bussiness_types', 'action' => 'index'] );?>"><i class="fa fa-table fa-fw"></i> Category</a>
                            </li>
                            <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'bussiness_sub_types', 'action' => 'index'] );?>"><i class="fa fa-table fa-fw"></i> Sub Category</a>
                            </li>
                            <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'feed_backs', 'action' => 'index'] );?>"><i class="fa fa-table fa-fw"></i>Feedback</a>
                            </li>
                            <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'reports', 'action' => 'index'] );?>"><i class="fa fa-table fa-fw"></i> Reports</a>
                            </li>
                            <li>
                                <a href="<?=$this->Url->build(['prefix' => "admin", 'controller' => 'users', 'action' => 'index'] );?>"><i class="fa fa-table fa-fw"></i> Users</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>