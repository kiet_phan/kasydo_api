<div class="container">
    <a class="site-logo" href="index.html"><img src="styles/assets/frontend/layout/img/logos/logo-shop-red.png" alt="Metronic Shop UI"></a>

    <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

    <!-- BEGIN NAVIGATION -->
    <div class="header-navigation">
        <ul>
            <li><a href="index.html">Trang Chủ</a></li>
            <?= $this->Menu->renderMenu($newCates,['controller'=>'new_cate','action'=>'show']) ?>
             <?= $this->Menu->renderMenu($categories,['controller'=>'categoris','action'=>'show']) ?>
            <li class="">
                <a  href="#">
                    Hỗ trợ

                </a>

<!--                <ul class="dropdown-menu">-->
<!--                    <li class="active"><a href="shop-index.html">Hư hỏng phần máy</a></li>-->
<!--                    <li><a href="shop-index-header-fix.html">Hư hỏng phần gầm</a></li>-->
<!--                    <li><a href="shop-index-light-footer.html">Hư hỏng phần điện</a></li>-->
<!--                    <li><a href="shop-product-list.html">Hư hỏng phần đồng sơn</a></li>-->
<!--                </ul>-->
            </li>


            <li class="">
                <a  href="#">
                    Liên Hệ

                </a>
            </li>
            <!-- BEGIN TOP SEARCH -->
            <li class="menu-search">
                <span class="sep"></span>
                <i class="fa fa-search search-btn"></i>
                <div class="search-box">
                    <form action="#">
                        <div class="input-group">
                            <input type="text" placeholder="Search" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                </div> 
            </li>
            <!-- END TOP SEARCH -->
        </ul>
    </div>
    <!-- END NAVIGATION -->
</div>
