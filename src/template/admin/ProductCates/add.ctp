
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Loại sản phẩm
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="index.html">Thêm loại sản phẩm</a>
            </li>
            <li class="active">
                <i class="fa fa-edit"></i> Forms
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
<div class="col-lg-6">
    <h1>Nhập thông tin</h1>

    <?=
        $this->Form->create($product_cate, [
            'horizontal' => true,
            'cols' => [
                'sm' => [
                    'label' => 4,
                    'input' => 4,
                    'error' => 4
                ],
                'md' => [
                    'label' => 2,
                    'input' => 6,
                    'error' => 4
                ]
            ]
        ])
    ?>
     <?= $this->Form->input('parent_id', [
                                'type' => 'select',
                                'label' => 'Danh mục cha',
                                'options' => $parrent_list
                            ])
    ?>
    <?= $this->Form->input('name', [
                                                'type' => 'text',
                                                'label' => 'Tên Danh mục'
                                            ])
    ?>
    <?= $this->Form->submit('Lưu') ?>
    <?= $this->Form->end() ?>

</div>
</div>
<!-- /.row -->
