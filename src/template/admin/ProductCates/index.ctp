<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<?php
$url_add = $url_edit = $this->Url->build([
    "controller" => "product_cates",
    "action" => "add",
        ]);
?>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh mục sản phẩm

            </div>
            <div class="panel-body">
                <a href="<?= $url_add ?>" class="btn btn-success">Thêm <i class="fa fa-add"></i> </a>
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Tên</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $i = 0 ?>
                            <?php
                            foreach ($data as $key => $val):
                                $i ++;
                                $class = $i % 2 != 0 ? "odd" : "even";
                                $url_edit = $this->Url->build([
                                    "controller" => "product_cates",
                                    "action" => "edit",
                                    $val->id
                                ]);
                                $url_del = $this->Url->build([
                                    "controller" => "product_cates",
                                    "action" => "detete",
                                    $val->id
                                ]);
                                ?>
                                <tr class="<?= $class ?>">
                                    <td><?= $i ?></td>
                                    <td><?= $val->name ?></td>
                                    <td class="center" style="max-width: 40px">
                                        <a href="<?= $url_edit ?>" class="btn btn-success">Sửa <i class="fa fa-edit"></i> </a>
                                        <a href="<?= $url_del ?>" class="btn btn-danger del_target">Xóa <i class="fa fa-trash-o fa-lg"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<?= $this->Html->script('../admin/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>
<?= $this->Html->script('../admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') ?>