<h1>Businesses</h1>

<div class="buttons">
    <?= $this->Html->link('New', ['action' => 'add'], ['class' => 'btn btn-primary']);?>
</div>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<br>
<div class="row">
    <button type="submit" data-behavior="delete_checked" data-src="/admin/bussinesses/delete_all" class="btn btn-danger">Delete checked</button>
</div>

<div class="bussinesses index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th><input class="check_all" type="checkbox"></th>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th class="number"><?= $this->Paginator->sort('bussiness_sub_type_id') ?></th>
            <th class="number"><?= $this->Paginator->sort('user_id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('latitude') ?></th>
            <th><?= $this->Paginator->sort('longitude') ?></th>
            <th><?= $this->Paginator->sort('address') ?></th>
            <th class="actions" style="min-width: 300px"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($bussinesses as $bussiness): ?>
        <tr>
            <td><input class="input_checked" id= "bussiness_id_<?= $bussiness->id?>" type="checkbox"></td>
            <td class="number"><?= $this->Number->format($bussiness->id) ?></td>
            <td>
                <?= $bussiness->has('bussiness_sub_type') ? $this->Html->link($bussiness->bussiness_sub_type->name, ['controller' => 'BussinessSubTypes', 'action' => 'view', $bussiness->bussiness_sub_type->id]) : '' ?>
            </td>
            <td>
                <?= $bussiness->has('user') ? $this->Html->link($bussiness->user->id, ['controller' => 'Users', 'action' => 'view', $bussiness->user->id]) : '' ?>
            </td>
            <td><?= h($bussiness->name)?></td>
            <td><?= h($bussiness->latitude)?></td>
            <td><?= h($bussiness->longitude)?></td>
            <td><?= h($bussiness->address)?></td>
            <td class="actions">
                <?php if($bussiness->status == 0):?>
                    <?= $this->Html->link(__('Un suspend'), ['action' => 'change_status', $bussiness->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?php  else: ?>
                    <?= $this->Html->link(__('Suspend'), ['action' => 'change_status', $bussiness->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?php endif;?>
                <?= $this->Html->link(__('View'), ['action' => 'view', $bussiness->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bussiness->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bussiness->id), 'class' => 'btn btn-xs btn-danger']) ?>

            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
