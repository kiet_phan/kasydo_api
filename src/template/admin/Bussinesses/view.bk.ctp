<div class="bussinesses view">
    <h2><?= h($bussiness->name) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Bussiness Sub Type') ?></h3></div>
                <div class="panel-body"><?= $bussiness->has('bussiness_sub_type') ? $this->Html->link($bussiness->bussiness_sub_type->name, ['controller' => 'BussinessSubTypes', 'action' => 'view', $bussiness->bussiness_sub_type->id]) : '' ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('User') ?></h3></div>
                <div class="panel-body"><?= $bussiness->has('user') ? $this->Html->link($bussiness->user->id, ['controller' => 'Users', 'action' => 'view', $bussiness->user->id]) : '' ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Name') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Latitude') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->latitude) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Longitude') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->longitude) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Address') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->address) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('District') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->district) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('City') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->city) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Province') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->province) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Image Name') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->image_name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Mime') ?></h3></div>
                <div class="panel-body"><?= h($bussiness->mime) ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($bussiness->id) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Size') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($bussiness->size) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($bussiness->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Updated At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($bussiness->updated_at) ?></div>
            </div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Products') ?></h4>
    <?php if (!empty($bussiness->products)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Title') ?></th>
                                    <th><?= __('Content') ?></th>
                                    <th><?= __('Price') ?></th>
                                    <th><?= __('Rating') ?></th>
                                    <th><?= __('Bussiness Id') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Updated At') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bussiness->products as $products): ?>
            <tr>
                                    <td><?= h($products->id) ?></td>
                                                        <td><?= h($products->title) ?></td>
                                                        <td><?= h($products->content) ?></td>
                                                        <td><?= h($products->price) ?></td>
                                                        <td><?= h($products->rating) ?></td>
                                                        <td><?= h($products->bussiness_id) ?></td>
                                                        <td><?= h($products->created_at) ?></td>
                                                        <td><?= h($products->updated_at) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id], ['class' => 'btn btn-xs btn-default']) ?>

                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Comments') ?></h4>
    <?php if (!empty($bussiness->comments)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Content') ?></th>
                                    <th><?= __('Commentable Id') ?></th>
                                    <th><?= __('User Id') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Modified At') ?></th>
                                    <th><?= __('Commentable Type') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bussiness->comments as $comments): ?>
            <tr>
                                    <td><?= h($comments->id) ?></td>
                                                        <td><?= h($comments->content) ?></td>
                                                        <td><?= h($comments->commentable_id) ?></td>
                                                        <td><?= h($comments->user_id) ?></td>
                                                        <td><?= h($comments->created_at) ?></td>
                                                        <td><?= h($comments->modified_at) ?></td>
                                                        <td><?= h($comments->commentable_type) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Comments', 'action' => 'view', $comments->id], ['class' => 'btn btn-xs btn-default']) ?>

                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
