<div class="bussinesses form">
    <?= $this->Form->create($bussiness) ?>
    <fieldset>
        <legend><?= __('Add Bussiness') ?></legend>
        <?php
        echo $this->Form->input('bussiness_sub_type_id', ['options' => $bussinessSubTypes, 'empty' => true]);
        echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
        echo $this->Form->input('name');
        echo $this->Form->input('latitude');
        echo $this->Form->input('longitude');
        echo $this->Form->input('address');
        echo $this->Form->input('district');
        echo $this->Form->input('city');
        echo $this->Form->input('province');
        echo $this->Form->input('created_at');
        echo $this->Form->input('updated_at');
//        echo $this->Form->input('image_name');
//        echo $this->Form->input('mime');
//        echo $this->Form->input('size');

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
