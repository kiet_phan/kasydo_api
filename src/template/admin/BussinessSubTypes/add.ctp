<div class="bussinessSubTypes form">
    <?= $this->Form->create($bussinessSubType) ?>
    <fieldset>
        <legend><?= __('Add Bussiness Sub Type') ?></legend>
        <?php
        echo $this->Form->input('name');         echo $this->Form->input('bussiness_type_id', ['options' => $bussinessTypes]);
        echo $this->Form->input('created_at');         echo $this->Form->input('updated_at');         ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
