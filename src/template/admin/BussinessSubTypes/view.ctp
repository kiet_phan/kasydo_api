<div class="bussinessSubTypes view">
    <h2><?= h($bussinessSubType->name) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Name') ?></h3></div>
                <div class="panel-body"><?= h($bussinessSubType->name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Bussiness Type') ?></h3></div>
                <div class="panel-body"><?= $bussinessSubType->has('bussiness_type') ? $this->Html->link($bussinessSubType->bussiness_type->name, ['controller' => 'BussinessTypes', 'action' => 'view', $bussinessSubType->bussiness_type->id]) : '' ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($bussinessSubType->id) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($bussinessSubType->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Updated At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($bussinessSubType->updated_at) ?></div>
            </div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Bussinesses') ?></h4>
    <?php if (!empty($bussinessSubType->bussinesses)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Bussiness Sub Type Id') ?></th>
                                    <th><?= __('User Id') ?></th>
                                    <th><?= __('Name') ?></th>
                                    <th><?= __('Latitude') ?></th>
                                    <th><?= __('Longitude') ?></th>
                                    <th><?= __('Address') ?></th>
                                    <th><?= __('District') ?></th>
                                    <th><?= __('City') ?></th>
                                    <th><?= __('Province') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Updated At') ?></th>
                                    <th><?= __('Image Name') ?></th>
                                    <th><?= __('Mime') ?></th>
                                    <th><?= __('Size') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bussinessSubType->bussinesses as $bussinesses): ?>
            <tr>
                                    <td><?= h($bussinesses->id) ?></td>
                                                        <td><?= h($bussinesses->bussiness_sub_type_id) ?></td>
                                                        <td><?= h($bussinesses->user_id) ?></td>
                                                        <td><?= h($bussinesses->name) ?></td>
                                                        <td><?= h($bussinesses->latitude) ?></td>
                                                        <td><?= h($bussinesses->longitude) ?></td>
                                                        <td><?= h($bussinesses->address) ?></td>
                                                        <td><?= h($bussinesses->district) ?></td>
                                                        <td><?= h($bussinesses->city) ?></td>
                                                        <td><?= h($bussinesses->province) ?></td>
                                                        <td><?= h($bussinesses->created_at) ?></td>
                                                        <td><?= h($bussinesses->updated_at) ?></td>
                                                        <td><?= h($bussinesses->image_name) ?></td>
                                                        <td><?= h($bussinesses->mime) ?></td>
                                                        <td><?= h($bussinesses->size) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bussinesses', 'action' => 'view', $bussinesses->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bussinesses', 'action' => 'edit', $bussinesses->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bussinesses', 'action' => 'delete', $bussinesses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bussinesses->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
