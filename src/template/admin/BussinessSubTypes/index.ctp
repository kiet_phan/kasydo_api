<h1>Sub Category</h1>

<div class="buttons">
    <?= $this->Html->link('New', ['action' => 'add'], ['class' => 'btn btn-primary']);?>
</div>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<div class="bussinessSubTypes index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th class="number"><?= $this->Paginator->sort('bussiness_type_id') ?></th>
            <th class="time"><?= $this->Paginator->sort('created_at') ?></th>
            <th class="time"><?= $this->Paginator->sort('updated_at') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($bussinessSubTypes as $bussinessSubType): ?>
        <tr>
            <td class="number"><?= $this->Number->format($bussinessSubType->id) ?></td>
            <td><?= h($bussinessSubType->name)?></td>
            <td>
                <?= $bussinessSubType->has('bussiness_type') ? $this->Html->link($bussinessSubType->bussiness_type->name, ['controller' => 'BussinessTypes', 'action' => 'view', $bussinessSubType->bussiness_type->id]) : '' ?>
            </td>
            <td class="time"><?= $this->Time->timeAgoInWords($bussinessSubType->created_at) ?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($bussinessSubType->updated_at) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $bussinessSubType->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bussinessSubType->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bussinessSubType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bussinessSubType->id), 'class' => 'btn btn-xs btn-danger']) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
