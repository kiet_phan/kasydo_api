<h1><?= $this->request->controller?></h1>

<div class="buttons">
    <?= $this->Html->link('New', ['action' => 'add'], ['class' => 'btn btn-primary']);?>
</div>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<div class="comments index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th class="number"><?= $this->Paginator->sort('commentable_id') ?></th>
            <th class="number"><?= $this->Paginator->sort('user_id') ?></th>
            <th class="time"><?= $this->Paginator->sort('created_at') ?></th>
            <th class="time"><?= $this->Paginator->sort('modified_at') ?></th>
            <th><?= $this->Paginator->sort('commentable_type') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($comments as $comment): ?>
        <tr>
            <td class="number"><?= $this->Number->format($comment->id) ?></td>
            <td>
                <?= $comment->has('product') ? $this->Html->link($comment->product->title, ['controller' => 'Products', 'action' => 'view', $comment->product->id]) : '' ?>
            </td>
            <td>
                <?= $comment->has('user') ? $this->Html->link($comment->user->id, ['controller' => 'Users', 'action' => 'view', $comment->user->id]) : '' ?>
            </td>
            <td class="time"><?= $this->Time->timeAgoInWords($comment->created_at) ?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($comment->modified_at) ?></td>
            <td><?= h($comment->commentable_type)?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $comment->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $comment->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $comment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comment->id), 'class' => 'btn btn-xs btn-danger']) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
