<div class="comments view">
    <h2><?= h($comment->id) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Product') ?></h3></div>
                <div class="panel-body"><?= $comment->has('product') ? $this->Html->link($comment->product->title, ['controller' => 'Products', 'action' => 'view', $comment->product->id]) : '' ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('User') ?></h3></div>
                <div class="panel-body"><?= $comment->has('user') ? $this->Html->link($comment->user->id, ['controller' => 'Users', 'action' => 'view', $comment->user->id]) : '' ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Commentable Type') ?></h3></div>
                <div class="panel-body"><?= h($comment->commentable_type) ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($comment->id) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($comment->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Modified At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($comment->modified_at) ?></div>
            </div>
        </div>
    </div>
    <div class="texts">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?= __('Content') ?></h3></div>
            <div class="panel-body"><?= $this->Text->autoParagraph(h($comment->content)) ?></div>
        </div>
    </div>
</div>
