<div class="comments form">
    <?= $this->Form->create($comment) ?>
    <fieldset>
        <legend><?= __('Edit Comment') ?></legend>
        <?php
        echo $this->Form->input('content');         echo $this->Form->input('commentable_id', ['options' => $products]);
        echo $this->Form->input('user_id', ['options' => $users]);
        echo $this->Form->input('created_at');         echo $this->Form->input('modified_at');         echo $this->Form->input('commentable_type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
