<h1><?= $this->request->controller?></h1>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<div class="reports index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th class="number"><?= $this->Paginator->sort('type_id') ?></th>
            <th><?= $this->Paginator->sort('type_name') ?></th>
            <th class="number"><?= $this->Paginator->sort('user_id') ?></th>
            <th class="time"><?= $this->Paginator->sort('created') ?></th>
            <th class="time"><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($reports as $report): ?>
        <tr>
            <td class="number"><?= $this->Number->format($report->id) ?></td>
            <td class="number"><?= $this->Number->format($report->type_id) ?></td>
            <td><?= h($report->type_name)?></td>
            <td>
                <?= $report->has('user') ? $this->Html->link($report->user->id, ['controller' => 'Users', 'action' => 'view', $report->user->id]) : '' ?>
            </td>
            <td class="time"><?= $this->Time->timeAgoInWords($report->created) ?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($report->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $report->id], ['class' => 'btn btn-default btn-xs']) ?>

            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
