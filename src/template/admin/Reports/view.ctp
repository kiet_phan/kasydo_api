<div class="reports view">
    <h2><?= h($report->id) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Type Name') ?></h3></div>
                <div class="panel-body"><?= h($report->type_name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('User') ?></h3></div>
                <div class="panel-body"><?= $report->has('user') ? $this->Html->link($report->user->id, ['controller' => 'Users', 'action' => 'view', $report->user->id]) : '' ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($report->id) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Type Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($report->type_id) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($report->created) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Modified') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($report->modified) ?></div>
            </div>
        </div>
    </div>
    <div class="texts">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?= __('Reason') ?></h3></div>
            <div class="panel-body"><?= $this->Text->autoParagraph(h($report->reason)) ?></div>
        </div>
    </div>
</div>
