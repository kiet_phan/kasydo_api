<div class="admins form">
    <?= $this->Form->create($admin) ?>
    <fieldset>
        <legend><?= __('Đổi mật khẩu ') ?></legend>
        <?php
        echo $this->Form->input('password');
        echo $this->Form->input('confirm_password',['type' => 'password']);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
<!--<h1>Form Validation States</h1>-->
<!--<form role="form">-->
<!--    <div class="form-group has-success">-->
<!--        <label class="control-label" for="inputSuccess">Input with success</label>-->
<!--        <input type="text" class="form-control" id="inputSuccess">-->
<!--    </div>-->
<!--    <div class="form-group has-warning">-->
<!--        <label class="control-label" for="inputWarning">Input with warning</label>-->
<!--        <input type="text" class="form-control" id="inputWarning">-->
<!--    </div>-->
<!--    <div class="form-group has-error">-->
<!--        <label class="control-label" for="inputError">Input with error</label>-->
<!--        <input type="text" class="form-control" id="inputError">-->
<!--    </div>-->
<!--</form>-->