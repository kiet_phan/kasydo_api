<?= $this->Flash->render() ?>
<div class="login-panel panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Please Sign In</h3>
    </div>
    <div class="panel-body">
        <form role="form" method="post">
            <fieldset>
                <div class="form-group">
                    <input class="form-control" placeholder="Email" name="email" type="text" autofocus>
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password_fake" class="hidden" autocomplete="off" style="display: none;">
                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                </div>
                <div class="checkbox">
                    <label>
                        <input name="data[Admin][remember]" type="checkbox" value="1">Remember Me
                    </label>
                </div>
                <!-- Change this to a button or input when using this as a form -->
                <input type="submit" class="btn btn-lg btn-success btn-block" value="Login">
            </fieldset>
        </form>
    </div>
</div>