<h1><?= $this->request->controller?></h1>

<div class="buttons">
    <?= $this->Html->link('New', ['action' => 'add'], ['class' => 'btn btn-primary']);?>
</div>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<div class="admins index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th class="time"><?= $this->Paginator->sort('created_at') ?></th>
            <th class="time"><?= $this->Paginator->sort('modified_at') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($admins as $admin): ?>
        <tr>
            <td class="number"><?= $this->Number->format($admin->id) ?></td>
            <td><?= h($admin->name)?></td>
            <td><?= h($admin->email)?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($admin->created_at) ?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($admin->modified_at) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $admin->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Html->link(__('Change password'), ['action' => 'change_password', $admin->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $admin->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $admin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $admin->id), 'class' => 'btn btn-xs btn-danger']) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
