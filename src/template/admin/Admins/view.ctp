<div class="admins view">
    <h2><?= h($admin->name) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Name') ?></h3></div>
                <div class="panel-body"><?= h($admin->name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Email') ?></h3></div>
                <div class="panel-body"><?= h($admin->email) ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($admin->id) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($admin->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Modified At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($admin->modified_at) ?></div>
            </div>
        </div>
    </div>
    <div class="texts">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?= __('Description') ?></h3></div>
            <div class="panel-body"><?= $this->Text->autoParagraph(h($admin->description)) ?></div>
        </div>
    </div>
</div>
