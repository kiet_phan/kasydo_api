<h1><?= $this->request->controller?></h1>


<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>
<br>
<div class="row">
    <button type="submit" data-behavior="delete_checked" data-src="/admin/users/delete_all" class="btn btn-danger">Delete checked</button>
</div>

<div class="users index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th ><input class="check_all" type="checkbox"></th>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('first_name') ?></th>
            <th><?= $this->Paginator->sort('last_name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('username') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user): ?>
        <tr>
            <td ><input class="input_checked" id= "user_id_<?= $user->id?>" type="checkbox"></td>
            <td class="number"><?= $this->Number->format($user->id) ?></td>
            <td><?= h($user->first_name)?></td>
            <td><?= h($user->last_name)?></td>
            <td><?= h($user->email)?></td>
            <td><?= h($user->username)?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'btn btn-xs btn-danger']) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
