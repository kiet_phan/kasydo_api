<div class="users view">
    <h2><?= h($user->id) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('First Name') ?></h3></div>
                <div class="panel-body"><?= h($user->first_name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Last Name') ?></h3></div>
                <div class="panel-body"><?= h($user->last_name) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Email') ?></h3></div>
                <div class="panel-body"><?= h($user->email) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Username') ?></h3></div>
                <div class="panel-body"><?= h($user->username) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Client') ?></h3></div>
                <div class="panel-body"><?= h($user->client) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Fb Id') ?></h3></div>
                <div class="panel-body"><?= h($user->fb_id) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Profile Image') ?></h3></div>
                <div class="panel-body"><?= h($user->profile_image) ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($user->id) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Expiry') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($user->expiry) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Active') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($user->active) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Birthday') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($user->birthday) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($user->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Updated At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($user->updated_at) ?></div>
            </div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Addresses') ?></h4>
    <?php if (!empty($user->addresses)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Country') ?></th>
                                    <th><?= __('City') ?></th>
                                    <th><?= __('Province') ?></th>
                                    <th><?= __('District') ?></th>
                                    <th><?= __('Ward') ?></th>
                                    <th><?= __('Phone') ?></th>
                                    <th><?= __('Home Phone') ?></th>
                                    <th><?= __('User Id') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Updated At') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->addresses as $addresses): ?>
            <tr>
                                    <td><?= h($addresses->id) ?></td>
                                                        <td><?= h($addresses->country) ?></td>
                                                        <td><?= h($addresses->city) ?></td>
                                                        <td><?= h($addresses->province) ?></td>
                                                        <td><?= h($addresses->district) ?></td>
                                                        <td><?= h($addresses->ward) ?></td>
                                                        <td><?= h($addresses->phone) ?></td>
                                                        <td><?= h($addresses->home_phone) ?></td>
                                                        <td><?= h($addresses->user_id) ?></td>
                                                        <td><?= h($addresses->created_at) ?></td>
                                                        <td><?= h($addresses->updated_at) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Addresses', 'action' => 'view', $addresses->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Addresses', 'action' => 'edit', $addresses->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Addresses', 'action' => 'delete', $addresses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $addresses->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Bussinesses') ?></h4>
    <?php if (!empty($user->bussinesses)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Bussiness Sub Type Id') ?></th>
                                    <th><?= __('User Id') ?></th>
                                    <th><?= __('Name') ?></th>
                                    <th><?= __('Latitude') ?></th>
                                    <th><?= __('Longitude') ?></th>
                                    <th><?= __('Address') ?></th>
                                    <th><?= __('District') ?></th>
                                    <th><?= __('City') ?></th>
                                    <th><?= __('Province') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Updated At') ?></th>
                                    <th><?= __('Image Name') ?></th>
                                    <th><?= __('Mime') ?></th>
                                    <th><?= __('Size') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->bussinesses as $bussinesses): ?>
            <tr>
                                    <td><?= h($bussinesses->id) ?></td>
                                                        <td><?= h($bussinesses->bussiness_sub_type_id) ?></td>
                                                        <td><?= h($bussinesses->user_id) ?></td>
                                                        <td><?= h($bussinesses->name) ?></td>
                                                        <td><?= h($bussinesses->latitude) ?></td>
                                                        <td><?= h($bussinesses->longitude) ?></td>
                                                        <td><?= h($bussinesses->address) ?></td>
                                                        <td><?= h($bussinesses->district) ?></td>
                                                        <td><?= h($bussinesses->city) ?></td>
                                                        <td><?= h($bussinesses->province) ?></td>
                                                        <td><?= h($bussinesses->created_at) ?></td>
                                                        <td><?= h($bussinesses->updated_at) ?></td>
                                                        <td><?= h($bussinesses->image_name) ?></td>
                                                        <td><?= h($bussinesses->mime) ?></td>
                                                        <td><?= h($bussinesses->size) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bussinesses', 'action' => 'view', $bussinesses->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bussinesses', 'action' => 'edit', $bussinesses->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bussinesses', 'action' => 'delete', $bussinesses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bussinesses->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
