<div class="users form">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('email');
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('old_password');
        echo $this->Form->input('acess_token');
        echo $this->Form->input('token');
        echo $this->Form->input('client');
        echo $this->Form->input('expiry');
        echo $this->Form->input('fb_id');
        echo $this->Form->input('profile_image');
        echo $this->Form->input('birthday', ['empty' => true, 'default' => '']);
        echo $this->Form->input('active');
        echo $this->Form->input('created_at');
        echo $this->Form->input('updated_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
