<h1><?= $this->request->controller?></h1>

<div class="buttons">
    <?= $this->Html->link('New', ['action' => 'add'], ['class' => 'btn btn-primary']);?>
</div>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<div class="products index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('title') ?></th>
            <th class="number"><?= $this->Paginator->sort('price') ?></th>
            <th class="number"><?= $this->Paginator->sort('rating') ?></th>
            <th class="number"><?= $this->Paginator->sort('bussiness_id') ?></th>
            <th class="time"><?= $this->Paginator->sort('created_at') ?></th>
            <th class="time"><?= $this->Paginator->sort('updated_at') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product): ?>
        <tr>
            <td class="number"><?= $this->Number->format($product->id) ?></td>
            <td><?= h($product->title)?></td>
            <td class="number"><?= $this->Number->format($product->price) ?></td>
            <td class="number"><?= $this->Number->format($product->rating) ?></td>
            <td>
                <?= $product->has('bussiness') ? $this->Html->link($product->bussiness->name, ['controller' => 'Bussinesses', 'action' => 'view', $product->bussiness->id]) : '' ?>
            </td>
            <td class="time"><?= $this->Time->timeAgoInWords($product->created_at) ?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($product->updated_at) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $product->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product->id], ['class' => 'btn btn-default btn-xs']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id), 'class' => 'btn btn-xs btn-danger']) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
