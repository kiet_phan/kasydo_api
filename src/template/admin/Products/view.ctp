<div class="products view">
    <h2><?= h($product->title) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Title') ?></h3></div>
                <div class="panel-body"><?= h($product->title) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Bussiness') ?></h3></div>
                <div class="panel-body"><?= $product->has('bussiness') ? $this->Html->link($product->bussiness->name, ['controller' => 'Bussinesses', 'action' => 'view', $product->bussiness->id]) : '' ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($product->id) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Price') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($product->price) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Rating') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($product->rating) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($product->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Updated At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($product->updated_at) ?></div>
            </div>
        </div>
    </div>
    <div class="texts">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?= __('Content') ?></h3></div>
            <div class="panel-body"><?= $this->Text->autoParagraph(h($product->content)) ?></div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Images') ?></h4>
    <?php if (!empty($product->images)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Foreign Key') ?></th>
                                    <th><?= __('Model') ?></th>
                                    <th><?= __('Description') ?></th>
                                    <th><?= __('Field') ?></th>
                                    <th><?= __('Image Name') ?></th>
                                    <th><?= __('Mime') ?></th>
                                    <th><?= __('Size') ?></th>
                                    <th><?= __('Created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->images as $images): ?>
            <tr>
                                    <td><?= h($images->id) ?></td>
                                                        <td><?= h($images->foreign_key) ?></td>
                                                        <td><?= h($images->model) ?></td>
                                                        <td><?= h($images->description) ?></td>
                                                        <td><?= h($images->field) ?></td>
                                                        <td><?= h($images->image_name) ?></td>
                                                        <td><?= h($images->mime) ?></td>
                                                        <td><?= h($images->size) ?></td>
                                                        <td><?= $this->Time->timeAgoInWords($images->created) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Images', 'action' => 'view', $images->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Images', 'action' => 'edit', $images->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Images', 'action' => 'delete', $images->id], ['confirm' => __('Are you sure you want to delete # {0}?', $images->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Comments') ?></h4>
    <?php if (!empty($product->comments)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Content') ?></th>
                                    <th><?= __('Commentable Id') ?></th>
                                    <th><?= __('User Id') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Modified At') ?></th>
                                    <th><?= __('Commentable Type') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->comments as $comments): ?>
            <tr>
                                    <td><?= h($comments->id) ?></td>
                                                        <td><?= h($comments->content) ?></td>
                                                        <td><?= h($comments->commentable_id) ?></td>
                                                        <td><?= h($comments->user_id) ?></td>
                                                        <td><?= h($comments->created_at) ?></td>
                                                        <td><?= h($comments->modified_at) ?></td>
                                                        <td><?= h($comments->commentable_type) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Comments', 'action' => 'view', $comments->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Comments', 'action' => 'edit', $comments->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Comments', 'action' => 'delete', $comments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comments->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
