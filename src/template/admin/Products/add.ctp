<div class="products form">
    <?= $this->Form->create($product) ?>
    <fieldset>
        <legend><?= __('Add Product') ?></legend>
        <?php
        echo $this->Form->input('title');
        echo $this->Form->input('content');
        echo $this->Form->input('price');
        echo $this->Form->input('rating');
        echo $this->Form->input('bussiness_id', ['options' => $bussinesses, 'empty' => true]);
        echo $this->Form->input('created_at');
        echo $this->Form->input('updated_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
