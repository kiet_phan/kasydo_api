<div class="feedBacks view">
    <h2><?= h($feedBack->id) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Summary') ?></h3></div>
                <div class="panel-body"><?= h($feedBack->summary) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('User') ?></h3></div>
                <div class="panel-body"><?= $feedBack->has('user') ? $this->Html->link($feedBack->user->id, ['controller' => 'Users', 'action' => 'view', $feedBack->user->id]) : '' ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($feedBack->id) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($feedBack->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Modified At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($feedBack->modified_at) ?></div>
            </div>
        </div>
    </div>
    <div class="texts">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?= __('Content') ?></h3></div>
            <div class="panel-body"><?= $this->Text->autoParagraph(h($feedBack->content)) ?></div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related Images') ?></h4>
    <?php if (!empty($feedBack->images)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Foreign Key') ?></th>
                                    <th><?= __('Model') ?></th>
                                    <th><?= __('Description') ?></th>
                                    <th><?= __('Field') ?></th>
                                    <th><?= __('Image Name') ?></th>
                                    <th><?= __('Mime') ?></th>
                                    <th><?= __('Size') ?></th>
                                    <th><?= __('Created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($feedBack->images as $images): ?>
            <tr>
                                    <td><?= h($images->id) ?></td>
                                                        <td><?= h($images->foreign_key) ?></td>
                                                        <td><?= h($images->model) ?></td>
                                                        <td><?= h($images->description) ?></td>
                                                        <td><?= h($images->field) ?></td>
                                                        <td><?= h($images->image_name) ?></td>
                                                        <td><?= h($images->mime) ?></td>
                                                        <td><?= h($images->size) ?></td>
                                                        <td><?= $this->Time->timeAgoInWords($images->created) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Images', 'action' => 'view', $images->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Images', 'action' => 'edit', $images->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Images', 'action' => 'delete', $images->id], ['confirm' => __('Are you sure you want to delete # {0}?', $images->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
