<h1><?= $this->request->controller?></h1>

<div class="filter">
    <?php
    echo $this->Form->create(null, ['class' => 'form-inline']);
    echo $this->Form->input('title');
    echo $this->Form->button('Filter',['type' => 'submit', 'class' => 'btn btn-success']);
    echo $this->Html->link('Reset', ['action' => 'index'], ['class' => 'btn btn-default']);
    echo $this->Form->end();
    ?>
</div>

<div class="feedBacks index">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="number"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('summary') ?></th>
            <th class="number"><?= $this->Paginator->sort('user_id') ?></th>
            <th class="time"><?= $this->Paginator->sort('created_at') ?></th>
            <th class="time"><?= $this->Paginator->sort('modified_at') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($feedBacks as $feedBack): ?>
        <tr>
            <td class="number"><?= $this->Number->format($feedBack->id) ?></td>
            <td><?= h($feedBack->summary)?></td>
            <td>
                <?= $feedBack->has('user') ? $this->Html->link($feedBack->user->id, ['controller' => 'Users', 'action' => 'view', $feedBack->user->id]) : '' ?>
            </td>
            <td class="time"><?= $this->Time->timeAgoInWords($feedBack->created_at) ?></td>
            <td class="time"><?= $this->Time->timeAgoInWords($feedBack->modified_at) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $feedBack->id], ['class' => 'btn btn-default btn-xs']) ?>

            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>

    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
</div>
