<div class="feedBacks form">
    <?= $this->Form->create($feedBack) ?>
    <fieldset>
        <legend><?= __('Add Feed Back') ?></legend>
        <?php
        echo $this->Form->input('summary');         echo $this->Form->input('content');         echo $this->Form->input('user_id', ['options' => $users]);
        echo $this->Form->input('created_at');
        echo $this->Form->input('modified_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
