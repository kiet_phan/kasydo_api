<div class="bussinessTypes form">
    <?= $this->Form->create($bussinessType, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Bussiness Type') ?></legend>
        <?php
        echo $this->Form->input('name');
        echo $this->Form->file('image');
        echo $this->Form->input('created_at');
        echo $this->Form->input('updated_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success']) ?>
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-default'])?>
    <?= $this->Form->end() ?>
</div>
