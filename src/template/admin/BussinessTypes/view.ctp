<div class="bussinessTypes view">
    <h2><?= h($bussinessType->name) ?></h2>
    <div class="non-text">
        <div class="strings">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Name') ?></h3></div>
                <div class="panel-body"><?= h($bussinessType->name) ?></div>
            </div>
        </div>
        <div class="numbers">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Id') ?></h3></div>
                <div class="panel-body"><?= $this->Number->format($bussinessType->id) ?></div>
            </div>
        </div>
        <div class="dates">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Created At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($bussinessType->created_at) ?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?= __('Updated At') ?></h3></div>
                <div class="panel-body"><?= $this->Time->timeAgoInWords($bussinessType->updated_at) ?></div>
            </div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-md-12">
    <h4 class="subheader"><?= __('Related BussinessSubTypes') ?></h4>
    <?php if (!empty($bussinessType->bussiness_sub_types)): ?>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
            <tr>
                                    <th><?= __('Id') ?></th>
                                    <th><?= __('Name') ?></th>
                                    <th><?= __('Bussiness Type Id') ?></th>
                                    <th><?= __('Created At') ?></th>
                                    <th><?= __('Updated At') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($bussinessType->bussiness_sub_types as $bussinessSubTypes): ?>
            <tr>
                                    <td><?= h($bussinessSubTypes->id) ?></td>
                                                        <td><?= h($bussinessSubTypes->name) ?></td>
                                                        <td><?= h($bussinessSubTypes->bussiness_type_id) ?></td>
                                                        <td><?= h($bussinessSubTypes->created_at) ?></td>
                                                        <td><?= h($bussinessSubTypes->updated_at) ?></td>
                    
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BussinessSubTypes', 'action' => 'view', $bussinessSubTypes->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BussinessSubTypes', 'action' => 'edit', $bussinessSubTypes->id], ['class' => 'btn btn-xs btn-default']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BussinessSubTypes', 'action' => 'delete', $bussinessSubTypes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bussinessSubTypes->id), 'class' => 'btn btn-xs btn-danger']) ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No related records found.</div>
    <?php endif;?>
    </div>
</div>
