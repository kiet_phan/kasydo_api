<?= $this->Flash->render(); ?>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Tin tức
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="index.html">Thêm Tin tức</a>
            </li>
            <li class="active">
                <i class="fa fa-edit"></i> Forms
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
<div class="col-lg-6">
    <h1>Nhập thông tin</h1>

    <?=
        $this->Form->create($new, [
            'type' => 'file'
            
        ])
    ?>
     <?= $this->Form->input('new_cate_id', [
                                'type' => 'select',
                                'label' => 'Loại bài viết',
                                'options' => $parrent_list
                            ])
    ?>
    <?= $this->Form->input('title', [
                                                'type' => 'text',
                                                'label' => 'Tiêu đề'
                                            ])
    ?>
     <?= $this->Form->input('new_sumary', [
                                                'type' => 'textarea',
                                                'label' => 'Nội Dung Tóm Tắt',
                                                'class' => 'form-control ckeditor',
                                                'div' => false
                                            ])
    ?>
     <?= $this->Form->input('new_content', [
                                                'type' => 'textarea',
                                                'class' => 'form-control ckeditor',
                                                'label' => 'Nội dung Chi Tiết'
                                            ])
    ?>
    
    <?php if($this->request->params['action'] == 'edit' && $new->image): ?>
    <div>
        <?= $this->Html->image('../admin/images/news/'.$new->id.'/thumb'.$new->image, ['alt' => $new->title,'class'=> "img-responsive"]); ?>
    </div>
    <?php endif;?>
    <div></div>
     <?= $this->Form->input('hinh', [
                                                'type' => 'file',
                                                'label' => 'Hình'
                                            ])
    ?>
     <?= $this->Form->input('san_pham_moi', [
                                                'type' => 'checkbox',
                                                'label' => 'Sản Phẩm mới'
                                            ])
    ?>
     <?= $this->Form->input('status', [
                                                'type' => 'checkbox',
                                                'label' => 'Trạng thái'
                                            ])
    ?>
    <?= $this->Form->submit('Lưu',['class'=>'btn btn-success']) ?>
    <?= $this->Form->end() ?>

</div>
</div>
<!-- /.row -->
