<?php
namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\Api\V1\BussinessesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\V1\BussinessesController Test Case
 */
class BussinessesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bussinesses',
        'app.bussiness_sub_types',
        'app.bussiness_types',
        'app.users',
        'app.addresses',
        'app.products'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
