<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\BussinessesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\BussinessesController Test Case
 */
class BussinessesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bussinesses',
        'app.bussiness_sub_types',
        'app.bussiness_types',
        'app.users',
        'app.addresses',
        'app.products',
        'app.images',
        'app.comments',
        'app.businesses'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
