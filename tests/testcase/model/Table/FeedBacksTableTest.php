<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FeedBacksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FeedBacksTable Test Case
 */
class FeedBacksTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.feed_backs',
        'app.users',
        'app.addresses',
        'app.bussinesses',
        'app.bussiness_sub_types',
        'app.bussiness_types',
        'app.products',
        'app.images',
        'app.comments',
        'app.businesses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FeedBacks') ? [] : ['className' => 'App\Model\Table\FeedBacksTable'];
        $this->FeedBacks = TableRegistry::get('FeedBacks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FeedBacks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
