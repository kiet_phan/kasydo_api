<?php
use Migrations\AbstractMigration;

class ChangeColumnNameInComments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
      $table = $this->table('comments');
      $table->renameColumn('product_id', 'commentable_id')
      ->addColumn('commentable_type', 'string', [
                'default' => null,
                'limit' => 60,
                'null' => true,
             ])
              ->update();;
    }
}
