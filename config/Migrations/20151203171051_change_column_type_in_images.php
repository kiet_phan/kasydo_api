<?php

use Migrations\AbstractMigration;

class ChangeColumnTypeInImages extends AbstractMigration {

  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function up() {
    $imageTable = $this->table('images');
    $imageTable->changeColumn('model', 'string', [
        'null' => true
      ])
      ->changeColumn('field', 'string', [
        'null' => true
      ])
      ->changeColumn('filename', 'string', [
        'null' => true
      ])
      ->changeColumn('mime', 'string', [
        'null' => true
      ])
      ->changeColumn('size', 'integer', [
        'null' => true
      ])
      ->update();
    $tableBussiness = $this->table('bussinesses');
    $tableBussiness->addColumn('image_name', 'string', ['null' => true])
      ->addColumn('mime', 'string', ['null' => true])
      ->addColumn('size', 'integer', ['null' => true])
      ->update();
    if ($this->hasTable('products')) {
      $table = $this->table('products');
      $table->rename('posts');
    }
  }

  public function down() {
    $imageTable = $this->table('images');
    $imageTable->changeColumn('model', 'string', [
        'null' => false
      ])
      ->changeColumn('field', 'string', [
        'null' => false
      ])
      ->changeColumn('filename', 'string', [
        'null' => false
      ])
      ->changeColumn('mime', 'string', [
        'null' => false
      ])
      ->changeColumn('size', 'integer', [
        'null' => false
      ])
      ->update();
    $tableBussiness = $this->table('bussinesses');
    $tableBussiness->removeColumn('image_name')
      ->removeColumn('mime')
      ->removeColumn('size')
      ->update();
    if ($this->hasTable('posts')) {
      $table = $this->table('posts');
      $table->rename('products');
    }
  }

}
