<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;
class AddPriceToProducts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('products');
        $table->addColumn('price', 'decimal', [
            'default' => null,
            'null' => true,
            'after'=> 'title'
        ]);
        $table->addColumn('content', 'text', [
            'default' => null,
            'null' => true,
            'after'=> 'title',
            'limit'=>MysqlAdapter::TEXT_LONG
        ]);
        $table->update();
    }
}
