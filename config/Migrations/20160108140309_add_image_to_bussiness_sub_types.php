<?php
use Migrations\AbstractMigration;

class AddImageToBussinessSubTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('bussiness_types');
        $table->addColumn('image_name', 'string', ['null' => true])
          ->addColumn('mime', 'string', ['null' => true])
          ->addColumn('size', 'integer', ['null' => true])
          ->update();
        $table->update();
    }
}
