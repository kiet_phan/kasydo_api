<?php
use Migrations\AbstractMigration;

class CreateFeedbacks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('feed_backs');
        $table->addColumn('summary', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('content', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('created_at', 'timestamp', [
          'default' => null,
          'null' => true,
        ]);
        $table->addColumn('modified_at', 'timestamp', [
          'default' => null,
          'limit' => 255,
          'null' => true,
        ]);
        $table->create();
    }
}
