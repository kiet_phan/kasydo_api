<?php
use Migrations\AbstractMigration;

class AddImageAbleTypeToImages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('images');
        $table->addColumn('image_able_type', 'string', [
            'default' => null,
            'limit' => 255,
            'after'=>'image_able_id',
            'null' => false,
        ]);
        $table->update();
    }
}
