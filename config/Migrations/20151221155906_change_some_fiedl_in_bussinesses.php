<?php
use Migrations\AbstractMigration;

class ChangeSomeFiedlInBussinesses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('bussinesses');
        $table->changeColumn('longitude',  'float',[
            'precision' => 10,
            'scale' => 6
        ])
          ->changeColumn('latitude',  'float',[
            'precision' => 10,
            'scale' => 6
          ])
          ->addColumn('page', 'string')
          ->addColumn('hour', 'string')
          ->addColumn('phone', 'string')

          ->update();
        $productTable = $this->table('products');
        $productTable->changeColumn('rating',  'integer',[
          'default' => 0
        ])
          ->changeColumn('price',  'decimal',[
            'default' => 0,
            'precision' => 10,
            'scale' => 6
          ])
            ->update();
    }
    public function down(){

    }
}
