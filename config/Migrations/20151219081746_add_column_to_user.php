<?php
use Migrations\AbstractMigration;

class AddColumnToUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('mime', 'string', [
            'default' => null,
            'limit' => 255,
            'after'=>'profile_image',
            'null' => true,
        ]);
        $table->addColumn('size', 'integer', [
            'default' => null,
            'limit' => 11,
            'after'=>'profile_image',
            'null' => true,
        ]);
        $table->update();
    }
}
