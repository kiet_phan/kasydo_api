<?php
use Migrations\AbstractMigration;

class ChangeDataTypeFromBussinesses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('bussinesses');
        $table->changeColumn('longitude',  'float')
          ->changeColumn('latitude',  'float')

          ->update();
    }
    public function down()
    {
        $table = $this->table('bussinesses');
        $table->changeColumn('longitude',  'float')
          ->changeColumn('latitude',  'float')

          ->update();
    }
}
