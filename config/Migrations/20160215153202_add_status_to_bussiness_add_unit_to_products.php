<?php
use Migrations\AbstractMigration;

class AddStatusToBussinessAddUnitToProducts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('bussinesses');
        $table->addColumn('status', 'boolean')

            ->update();
        $productTable = $this->table('products');
        $productTable->addColumn('unit',  'string',[
            'default' => 'VND'
        ])
            ->update();
    }
    
}
