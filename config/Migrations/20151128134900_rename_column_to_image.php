<?php

use Migrations\AbstractMigration;

class RenameColumnToImage extends AbstractMigration {

  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function up() {
    $table = $this->table('images');
    $table->renameColumn('image_able_type', 'imageable_type');
    $table->renameColumn('image_able_id', 'imageable_id');
  }

  public function down() {
    $table = $this->table('images');
    $table->renameColumn('imageable_type','image_able_type');
    $table->renameColumn( 'imageable_id','image_able_id');
  }

}
