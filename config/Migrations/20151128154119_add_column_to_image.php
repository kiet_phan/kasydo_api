<?php

use Migrations\AbstractMigration;

class AddColumnToImage extends AbstractMigration {

  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    $table = $this->table('images');
    $table->addColumn('mine', 'string', [
      'default' => null,
      'limit' => 255,
      'after' => 'imageable_id',
      'null' => false,
    ]);
    $table->addColumn('size', 'string', [
      'default' => null,
      'after' => 'imageable_id',
      'limit' => 255,
      'null' => false,
    ]);
    $table->addColumn('filename', 'string', [
      'default' => null,
      'after' => 'imageable_id',
      'limit' => 255,
      'null' => false,
    ]);
    $table->update();
  }

}
