<?php

use Migrations\AbstractMigration;

class ChangeTableName extends AbstractMigration {

  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function up() {
    $tableComment = $this->table('comments');
    $tableComment->addColumn('content', 'text')
      ->addColumn('product_id', 'integer')
      ->addColumn('user_id', 'integer')
      ->addColumn('created_at', 'datetime')
      ->addColumn('modified_at', 'datetime')
      ->create();

    $imageTable = $this->table('images');
    $imageTable->addColumn('description', 'text', [
      'after' => 'model'
    ])->update();
    if ($this->hasTable('posts')) {
      $table = $this->table('posts');
      $table->rename('products');
    }
  }

  public function down() {
    $table = $this->table('images');
    $table->drop();

    $imageTable = $this->table('images');
    $imageTable->removeColumn('description')->update();
    if ($this->hasTable('products')) {
      $table = $this->table('products');
      $table->rename('posts');
    }
  }

}
