<?php

use Migrations\AbstractMigration;

class DropTableImages extends AbstractMigration {

  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function up() {
    $table = $this->table('images');
    $table->drop();
  }

  public function down() {
    $table = $this->table('images', ['id' => false, 'primary_key' => ['id']]);
    $table
      ->addColumn('id', 'integer', ['identity' => true, 'signed' => false])
      ->addColumn('foreign_key', 'integer', ['signed' => false, 'null' => false])
      ->addColumn('model', 'string', ['limit' => 255, 'null' => false])
      ->addColumn('field', 'string', ['limit' => 255, 'null' => false])
      ->addColumn('filename', 'string', ['limit' => 255, 'null' => false])
      ->addColumn('mime', 'string', ['limit' => 255, 'null' => false])
      ->addColumn('size', 'integer', ['signed' => false, 'null' => false])
      ->addColumn('created', 'datetime', ['null' => false])
      ->save();
  }

}
