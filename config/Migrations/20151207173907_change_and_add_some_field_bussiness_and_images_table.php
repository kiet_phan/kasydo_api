<?php

use Migrations\AbstractMigration;

class ChangeAndAddSomeFieldBussinessAndImagesTable extends AbstractMigration {

  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function up() {
  
    $imageTable = $this->table('images');
    $imageTable->renameColumn('filename', 'image_name')->update();
    $buss_Table = $this->table('bussinesses');
    $buss_Table->addColumn('province', 'string', ['null' => true,'after'=>'longitude'])
      ->addColumn('city', 'string', ['null' => true,'after'=>'longitude'])
      ->addColumn('district', 'string', ['null' => true,'after'=>'longitude'])
      ->addColumn('address', 'string', ['null' => true,'after'=>'longitude'])
      ->update();
  }

  public function down() {
    $imageTable = $this->table('images');
    $imageTable->renameColumn('image_name','filename')->update();
    $buss_Table = $this->table('bussinesses');
    $buss_Table->removeColumn('province')
      ->removeColumn('city')
      ->removeColumn('district')
      ->removeColumn('address')
      ->update();
  }

}
