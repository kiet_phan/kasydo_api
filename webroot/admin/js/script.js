$(document).ready(function(){

    $('[data-behavior="delete_checked"]').click(function(){

        var ids = []
        var url = $(this).attr('data-src')
        $('.input_checked').each(function(index,element){
            if($(this).prop('checked')) {
                ids.push($(this).attr('id').split('_').pop())
            }

        });
        var r = confirm("Xoá tất cả các item "+ ids.join(' , '));
        if(r === false) return false;
        if(ids.length > 0){
            window.location.href = url + "?ids=" + ids.join(',')
        }

    });
    $('.check_all').change(function () {
        $("input.input_checked").prop('checked', $(this).prop("checked"));
    });
})